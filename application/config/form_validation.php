<?php

/**
 * Config file for form validation
 * http://www.codeigniter.com/user_guide/libraries/form_validation.html (Under section "Creating Sets of Rules")
 */

$config = array(

	/** Example: 
	'auth/login' => array(
		array(
			'field'		=> 'email',
			'label'		=> 'Email',
			'rules'		=> 'required|valid_email',
		),
		array(
			'field'		=> 'password',
			'label'		=> 'Password',
			'rules'		=> 'required',
		),
	),*/

	// Login
	'login/index' => array(
		array(
			'field'		=> 'email',
			'label'		=> 'Email',
			'rules'		=> 'required|valid_email',
		),
		array(
			'field'		=> 'password',
			'label'		=> 'Password',
			'rules'		=> 'required',
		),
	),

	// Sign Up
	'register/index' => array(
		array(
			'field'		=> 'full_name',
			'label'		=> 'Nama Lengkap',
			'rules'		=> 'required',
		),
		array(
			'field'		=> 'email',
			'label'		=> 'Email',
			'rules'		=> 'required|valid_email|is_unique[pencari_kerja.email]',
		),
		array(
			'field'		=> 'password',
			'label'		=> 'Password',
			'rules'		=> 'required|matches[retype_password]',
		),
		array(
			'field'		=> 'retype_password',
			'label'		=> 'Retype Password',
			'rules'		=> 'required',
		),
		array(
			'field'		=> 'termsandcondition',
			'label'		=> 'Terms And Conditions',
			'rules'		=> 'required',
		),
	),

	// Join Member and upload bukti pembayaran
	'register/join_member' => array(
		array(
			'field'		=> 'payment_method',
			'label'		=> 'Metode Pembayaran',
			'rules'		=> 'required',
		),
		// array(
		// 	'field'		=> 'payment_file',
		// 	'label'		=> 'Bukti Pembayaran',
		// 	'rules'		=> 'required',
		// ),
		array(
			'field'		=> 'payment_number',
			'label'		=> 'Nomor Rekening/ Nomor OVO',
			'rules'		=> 'required',
		),
		array(
			'field'		=> 'termsandcondition',
			'label'		=> 'Terms And Conditions',
			'rules'		=> 'required',
		),
	),

	// Forgot Password
	'account/forgot_password' => array(
		array(
			'field'		=> 'email',
			'label'		=> 'Email',
			'rules'		=> 'required|valid_email',
		),
	),

	// Reset Password
	'account/reset_password' => array(
		array(
			'field'		=> 'password',
			'label'		=> 'Password',
			'rules'		=> 'required|matches[retype_password]',
		),
		array(
			'field'		=> 'retype_password',
			'label'		=> 'Retype Password',
			'rules'		=> 'required',
		),
	),

	// Update Info
	'account/update_info' => array(
		array(
			'field'		=> 'first_name',
			'label'		=> 'First Name',
			'rules'		=> 'required',
		),
		array(
			'field'		=> 'last_name',
			'label'		=> 'Last Name',
			'rules'		=> 'required',
		),
		array(
			'field'		=> 'email',
			'label'		=> 'Email',
			'rules'		=> 'required|valid_email',
		),
	),

	// Change Password
	'account/change_password' => array(
		array(
			'field'		=> 'current_password',
			'label'		=> 'Current Password',
			'rules'		=> 'required',
		),
		array(
			'field'		=> 'new_password',
			'label'		=> 'New Password',
			'rules'		=> 'required|matches[retype_password]',
		),
		array(
			'field'		=> 'retype_password',
			'label'		=> 'Retype Password',
			'rules'		=> 'required',
		),
	),
);

/**
 * Google reCAPTCHA settings
 * https://www.google.com/recaptcha/
 */
$config['recaptcha'] = array(
	'site_key'		=> '',
	'secret_key'	=> '',
);
