<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| CI Bootstrap 3 Configuration
| -------------------------------------------------------------------------
| This file lets you define default values to be passed into views 
| when calling MY_Controller's render() function. 
| 
| See example and detailed explanation from:
| 	/application/config/ci_bootstrap_example.php
*/

$config['ci_bootstrap'] = array(

	// Site name
	'site_name' => 'Pandum Agritekno',

	// google api key
	'google_api_key' => 'AIzaSyAshH-WsVH1fx2b0nNdnqH-iSpWu66N9N4',

	// Default page title prefix
	'page_title_prefix' => '',

	// Default page title
	'page_title' => '',

	// Default meta data
	'meta_data'	=> array(
		'author'		=> '',
		'description'	=> '',
		'keywords'		=> ''
	),

	// Default scripts to embed at page head or end
	'scripts' => array(
		'head'	=> array(
			"themes/oficiona/assets/js/jquery.min.js",
		),
		'foot'	=> array(
			"themes/oficiona/assets/js/popper.min.js",
			"themes/oficiona/assets/js/bootstrap.min.js",
			"themes/oficiona/assets/js/feather.min.js",
			"themes/oficiona/assets/js/bootstrap-select.min.js",
			"themes/oficiona/assets/js/jquery.nstSlider.min.js",
			"themes/oficiona/assets/js/owl.carousel.min.js",
			"themes/oficiona/assets/js/visible.js",
			"themes/oficiona/assets/js/jquery.countTo.js",
			"themes/oficiona/assets/js/chart.js",
			"themes/oficiona/assets/js/plyr.js",
			"themes/oficiona/assets/js/tinymce.min.js",
			"themes/oficiona/assets/js/slick.min.js",
			"themes/oficiona/assets/js/jquery.ajaxchimp.min.js",

			"themes/oficiona/js/custom.js",
			"themes/oficiona/dashboard/js/dashboard.js",
			"themes/oficiona/dashboard/js/datePicker.js",
			"themes/oficiona/dashboard/js/upload-input.js",
			//"https://maps.googleapis.com/maps/api/js?key=AIzaSyC87gjXWLqrHuLKR0CTV5jNLdP4pEHMhmg",
			//"themes/oficiona/js/map.js"

			"assets/general/js/highlight.pack.min.js",
			"assets/general/js/clipboard.min.js",
		),
	),

	// Default stylesheets to embed at page head
	'stylesheets' => array(
		'screen' => array(
			"themes/oficiona/assets/css/bootstrap.min.css",
			"themes/oficiona/assets/css/fontawesome-all.min.css",
			"themes/oficiona/assets/css/themify-icons.css",
			"themes/oficiona/assets/css/et-line.css",
			"themes/oficiona/assets/css/bootstrap-select.min.css",
			"themes/oficiona/assets/css/plyr.css",
			"themes/oficiona/assets/css/flag.css",
			"themes/oficiona/assets/css/slick.css",
			"themes/oficiona/assets/css/owl.carousel.min.css",
			"themes/oficiona/assets/css/jquery.nstSlider.min.css",
			"themes/oficiona/css/main.css",
			"themes/oficiona/css/dashboard.css",

			"https://fonts.googleapis.com/css?family=Poppins:400,500,600%7CRoboto:300i,400,500",
		)
	),

	// Default CSS class for <body> tag
	'body_class' => 'bg-white',

	// Multilingual settings
	'languages' => array(
		/*'default'		=> 'en',
		'autoload'		=> array('general'),
		'available'		=> array(
			'en' => array(
				'label'	=> 'English',
				'value'	=> 'english'
			),
			'zh' => array(
				'label'	=> '繁體中文',
				'value'	=> 'traditional-chinese'
			),
			'cn' => array(
				'label'	=> '简体中文',
				'value'	=> 'simplified-chinese'
			),
			'es' => array(
				'label'	=> 'Español',
				'value' => 'spanish'
			)
		)*/),

	// Google Analytics User ID
	'ga_id' => '',

	// Menu items
	'menu' => array(
		'home' => array(
			'name'		=> 'Home',
			'url'		=> '',
		),
	),

	// Login page
	'login_url' => '',

	// Restricted pages
	'page_auth' => array(),

	// Email config
	'email' => array(
		'from_email'		=> '',
		'from_name'			=> '',
		'subject_prefix'	=> '',

		// Mailgun HTTP API
		'mailgun_api'		=> array(
			'domain'			=> '',
			'private_api_key'	=> ''
		),
	),

	// Debug tools
	'debug' => array(
		'view_data'	=> FALSE,
		'profiler'	=> FALSE
	),
);

/*
| -------------------------------------------------------------------------
| Override values from /application/config/config.php
| -------------------------------------------------------------------------
*/
$config['sess_cookie_name'] = 'sess_pandum_agritekno';
