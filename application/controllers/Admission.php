<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admission
 */
class Admission extends MY_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function save_admission()
    {
    	$admission = $this->input->post();
    	$admission['admission_id'] = null;
    	$admission['created_at'] = date('Y-m-d H:i:s');
    	$admission['ip_address'] = $this->input->ip_address();

    	$this->db->insert('admission', $admission);

    	set_alert('success', 'Success !! Thank you for registration');
    	redirect( base_url('admission') );
    }
} 