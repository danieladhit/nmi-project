<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Cronjob Process
 */
class Cronjob extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		echo 'Hello this is Cronjob';
		$this->db->insert('cronjob_test', array(
				'id' => null,
				'note' => 'testing cronjob2'
			));
		$calendar_events = $this->global->get_data('calendar_events');
	}

	public function meeting_invitation()
	{
		exit('cronjob invitation');
	}
	
}
