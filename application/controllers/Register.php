<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Register page
 */
class Register extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Pencari_kerja_model', 'pencaker');
		$this->load->model('Bkk_sekolah_model', 'bkk');
		$this->load->helper('email');
	}

	public function upload_image($pencaker_data)
	{
		$config['upload_path']          = './assets/uploads/payment_file/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 1000;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('payment_file')) {
			$error = array('error' => $this->upload->display_errors());
			set_alert('danger', $error['error']);

			redirect('register/join_member/' . $pencaker_data['activation_code'] . '/' . $pencaker_data['id']);
		} else {
			return $this->upload->data();
		}
	}

	public function index()
	{
		$this->nav = 'register';
		$this->nav_child = 'register';

		if (validate_form()) {
			$pencaker_data = elements(['full_name', 'email', 'id_bkk_sekolah', 'password', 'termsandcondition'], $this->input->post());
			$pencaker_data['password'] = hash_pw($pencaker_data['password']);

			// generate activation code
			$pencaker_data['activation_code'] = generate_unique_code();

			// confirm to create user
			$pencaker_id = $this->pencaker->insert($pencaker_data);

			if (!empty($pencaker_id)) {
				// get newly created user (with activation code)
				$pencaker = $this->pencaker->get($pencaker_id);

				// send activation email (make sure config/email.php is properly set first)
				/*
				$to_name = $user['first_name'].' '.$user['last_name'];
				$subject = 'Account Activation';
				send_email($user['email'], $to_name, $subject, 'signup', $user);
				*/

				// redirect with success message
				//set_alert('success', 'Proses register berhasil, silahkan');
				redirect('register/join_member/' . $pencaker_data['activation_code'] . '/' . $pencaker_id);
			}

			// failed
			set_alert('danger', 'Failed to create user.');
			redirect('register');
		}


		$bkk = $this->bkk->get_all();
		$this->mViewData['bkk'] = $bkk;

		$this->render('register', 'empty');
	}

	private function _save_login_session($pencaker) {
		// limited fields to store in session
		$fields = array('id', 'full_name', 'email', 'id_bkk_sekolah', 'payment_info', 'activation_code', 'payment_code', 'register_status', 'payment_status', 'member_status', 'created_at', 'updated_at');
		$pencaker_data = elements($fields, $pencaker);

		// save to user session
		login_user($pencaker_data);
	}

	public function join_member($activation_code = '', $user_id = 0, $process = 'signup')
	{
		// validasi parameter
		if (empty($activation_code) || empty($user_id)) {
			set_alert('danger', 'Silahkan register terlebih dahulu !!');
			redirect('register');
		}

		// search pencaker data
		$pencaker = $this->pencaker->get_by([
			'id'		=> $user_id,
			'activation_code' => $activation_code
		]);

		if (empty($pencaker)) {
			set_alert('danger', 'Maaf akun Anda tidak ditemukan silahkan register terlebih dahulu !!');
			redirect('register');
		}

		// save login session
		$this->_save_login_session((array) $pencaker);

		// jika sudah melakukan verifikasi alihkan ke halaman payment verification
		if ($pencaker->payment_status > 0) {
			redirect('register/payment_verification/' . $pencaker->payment_code);
		}

		// payment price
		$price = 25000;

		// generate payment unique id
		$payment_unique_id = substr($pencaker->id, -3);

		// generate total payment
		$total_payment = $price + $payment_unique_id;

		$this->mViewData['price'] = $price;
		$this->mViewData['payment_unique_id'] = $payment_unique_id;
		$this->mViewData['total_payment'] = $total_payment;

		if (validate_form()) {
			$payment_file = $this->upload_image((array) $pencaker);
			$payment_data = elements(['payment_method', 'payment_number'], $this->input->post());
			$payment_data['payment_file'] = $payment_file['file_name'];
			$payment_data['payment_amount'] = $total_payment;
			$payment_data['payment_date'] = date('Y-m-d H:i:s', now());

			// generate payment code
			$code_from_payment_date = str_replace('-', '', $payment_data['payment_date']); // replace - from date
			$code_from_payment_date = str_replace(':', '', $code_from_payment_date); // replace : from time
			$code_from_payment_date = str_replace(' ', '', $code_from_payment_date); // replace space from both

			$code_from_payment_amount = $payment_data['payment_amount'];

			$code_from_user_id = $pencaker->id;

			$payment_data['payment_code'] = $code_from_payment_date . $code_from_payment_amount . $code_from_user_id;

			$payment_data['payment_status'] = 1;

			// payment process = update user payment
			$this->pencaker->update($pencaker->id, $payment_data);

			set_alert('success', 'Proses register berhasil, silahkan');
			redirect('register/payment_verification/' . $payment_data['payment_code']);
		}

		$this->mViewData['pencaker'] = $pencaker;
		$this->mViewData['process'] = $process;
		$this->render('register/join_member', 'no_header');
	}

	public function payment_verification($payment_code)
	{
		// search pencaker data
		$pencaker = $this->pencaker->get_by([
			'payment_code' => $payment_code
		]);

		if (empty($pencaker)) {
			set_alert('danger', 'Maaf akun Anda tidak ditemukan silahkan register terlebih dahulu !!');
			redirect('register');
		}

		// jika status pembayaran sudah diverifikasi dan di approve oleh admin 
		if ($pencaker->payment_status == 2) {
			set_alert('success', 'Akun Anda sudah terverifikasi, selamat datang ' . $pencaker->full_name);
			redirect('pencari_kerja');
		}

		$this->mViewData['payment_code'] = $payment_code;
		$this->mViewData['pencaker'] = $pencaker;
		$this->render('register/payment_verification', 'no_header');
	}

	// Account activation
	public function register_aktivasi($code = '')
	{
		$user = (array) $this->pencaker->get_by([
			'activation_code' 	=> $code,
			'register_status'	=> 0
		]);

		// user found
		if (!empty($user) && !empty($code)) {
			// change user status
			$this->pencaker->update($user['id'], ['register_status' => 1]);

			// (optional) send welcome email
			//$to_name = $user['first_name'].' '.$user['last_name'];
			//$subject = 'Welcome';
			//send_email($user['email'], $to_name, $subject, 'welcome', $user);

			// success
			set_alert('success', 'Account activated! Please login your account to continue.');
			redirect('login');
			exit;
		}

		// failed
		set_alert('danger', 'Kode Aktivasi tidak valid !!');
		redirect('login');
	}

	function gauth($process = 'signup')
	{
		// Fill CLIENT ID, CLIENT SECRET ID, REDIRECT URI from Google Developer Console
		$client_id = '242496364465-cf5p49pqu830rblmku7fc83lrf8foglo.apps.googleusercontent.com';
		$client_secret = 'wWCPp7-aQC0e1rvq9qe0hj8z';
		$redirect_uri = base_url('register/gcallback/' . $process);

		//Create Client Request to access Google API
		$client = new Google_Client();
		$client->setApplicationName("Nano Mega Industri");
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);
		$client->setRedirectUri($redirect_uri);
		$client->addScope("email");
		$client->addScope("profile");

		//Send Client Request
		$objOAuthService = new Google_Service_Oauth2($client);

		$authUrl = $client->createAuthUrl();

		header('Location: ' . $authUrl);
	}

	function gcallback($process = 'signup')
	{
		// Fill CLIENT ID, CLIENT SECRET ID, REDIRECT URI from Google Developer Console
		$client_id = '242496364465-cf5p49pqu830rblmku7fc83lrf8foglo.apps.googleusercontent.com';
		$client_secret = 'wWCPp7-aQC0e1rvq9qe0hj8z';
		$redirect_uri = base_url('register/gcallback/' . $process);

		//Create Client Request to access Google API
		$client = new Google_Client();
		$client->setApplicationName("Nano Mega Industri");
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);
		$client->setRedirectUri($redirect_uri);
		$client->addScope("email");
		$client->addScope("profile");

		// //Send Client Request
		$service = new Google_Service_Oauth2($client);

		$client->authenticate($_GET['code']);
		$_SESSION['access_token'] = $client->getAccessToken();

		// User information retrieval starts..............................

		$user = $service->userinfo->get(); //get user info 

		// echo "</br> User ID :" . $user->id;
		// echo "</br> User Name :" . $user->name;
		// echo "</br> Gender :" . $user->gender;
		// echo "</br> User Email :" . $user->email;
		// echo "</br> User Link :" . $user->link;
		// echo "</br><img src='$user->picture'> ";

		// get user data with google id
		// search pencaker data
		$pencaker = $this->pencaker->get_by([
			'google_id' => $user->id,
		]);

		// jika sudah terdaftar, verify user
		if (!empty($pencaker)) {
			redirect('register/join_member/' . $pencaker->activation_code . '/' . $pencaker->id . '/' .$process);
		}

		// generate user data
		$pencaker_data['username'] = $user->name;
		$pencaker_data['full_name'] = $user->name;
		$pencaker_data['google_email'] = $user->email;
		$pencaker_data['google_id'] = $user->id;
		$pencaker_data['register_status'] = 1;

		// generate activation code
		$pencaker_data['activation_code'] = generate_unique_code();

		// confirm to create user
		$pencaker_id = $this->pencaker->insert($pencaker_data);

		if (!empty($pencaker_id)) {
			// get newly created user (with activation code)
			$pencaker = $this->pencaker->get($pencaker_id);

			// send activation email (make sure config/email.php is properly set first)
			/*
			$to_name = $user['first_name'].' '.$user['last_name'];
			$subject = 'Account Activation';
			send_email($user['email'], $to_name, $subject, 'signup', $user);
			*/

			// redirect with success message
			//set_alert('success', 'Proses register berhasil, silahkan');
			redirect('register/join_member/' . $pencaker_data['activation_code'] . '/' . $pencaker_id . '/' .$process);
		} else {
			set_alert('danger', 'Proses Gagal !! Akun Google Anda tidak valid');
			redirect('register');
		}
	}

	function fbauth($process = 'signup')
	{

		$fb = new Facebook\Facebook([
			'app_id' => '256984025442708',
			'app_secret' => 'b841696be6bcaff884a22982d47cc804',
			'default_graph_version' => 'v2.5',
		]);

		$helper = $fb->getRedirectLoginHelper();

		$permissions = ['email', 'user_location', 'user_birthday'];
		// For more permissions like user location etc you need to send your application for review

		$loginUrl = $helper->getLoginUrl(base_url().'register/fbcallback/' . $process, $permissions);

		header("location: " . $loginUrl);
	}

	function fbcallback($process = 'signup')
	{

		$fb = new Facebook\Facebook([
			'app_id' => '256984025442708',
			'app_secret' => 'b841696be6bcaff884a22982d47cc804',
			'default_graph_version' => 'v2.5',
		]);

		$helper = $fb->getRedirectLoginHelper();

		try {

			$accessToken = $helper->getAccessToken();
		} catch (Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error  
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch (Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues  
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}


		try {
			// Get the Facebook\GraphNodes\GraphUser object for the current user.
			// If you provided a 'default_access_token', the '{access-token}' is optional.
			$response = $fb->get('/me?fields=id,name,email,first_name,last_name,birthday,location,gender', $accessToken);
			// print_r($response);
		} catch (Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
			echo 'ERROR: Graph ' . $e->getMessage();
			exit;
		} catch (Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			echo 'ERROR: validation fails ' . $e->getMessage();
			exit;
		}

		// User Information Retrival begins................................................
		$user = $response->getGraphUser();

		// $location = $me->getProperty('location');
		// echo "Full Name: " . $me->getProperty('name') . "<br>";
		// echo "First Name: " . $me->getProperty('first_name') . "<br>";
		// echo "Last Name: " . $me->getProperty('last_name') . "<br>";
		// echo "Gender: " . $me->getProperty('gender') . "<br>";
		// echo "Email: " . $me->getProperty('email') . "<br>";
		// echo "location: " . $location['name'] . "<br>";
		// echo "Birthday: " . $me->getProperty('birthday')->format('d/m/Y') . "<br>";
		// echo "Facebook ID: <a href='https://www.facebook.com/" . $me->getProperty('id') . "' target='_blank'>" . $me->getProperty('id') . "</a>" . "<br>";
		// $profileid = $me->getProperty('id');
		// echo "</br><img src='//graph.facebook.com/$profileid/picture?type=large'> ";
		// echo "</br></br>Access Token : </br>" . $accessToken;

		// get user data with facebook id
		// search pencaker data
		$pencaker = $this->pencaker->get_by([
			'facebook_id' => $user->getProperty('id'),
		]);

		// jika sudah terdaftar, verify user
		if (!empty($pencaker)) {
			redirect('register/join_member/' . $pencaker->activation_code . '/' . $pencaker->id . '/' .$process);
		}

		// generate user data
		$pencaker_data['username'] = $user->getProperty('name');
		$pencaker_data['full_name'] = $user->getProperty('name');
		$pencaker_data['facebook_email'] = $user->getProperty('email');
		$pencaker_data['facebook_id'] = $user->getProperty('id');
		$pencaker_data['register_status'] = 1;

		// generate activation code
		$pencaker_data['activation_code'] = generate_unique_code();

		// confirm to create user
		$pencaker_id = $this->pencaker->insert($pencaker_data);

		if (!empty($pencaker_id)) {
			// get newly created user (with activation code)
			$pencaker = $this->pencaker->get($pencaker_id);

			// send activation email (make sure config/email.php is properly set first)
			/*
			$to_name = $user['first_name'].' '.$user['last_name'];
			$subject = 'Account Activation';
			send_email($user['email'], $to_name, $subject, 'signup', $user);
			*/

			// redirect with success message
			//set_alert('success', 'Proses register berhasil, silahkan');
			redirect('register/join_member/' . $pencaker_data['activation_code'] . '/' . $pencaker_id . '/' .$process);
		} else {
			set_alert('danger', 'Proses Gagal !! Akun Google Anda tidak valid');
			redirect('register');
		}
	}
}
