<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Pencari Kerja page
 */
class Pencari_kerja extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Pencari_kerja_model', 'pencaker');
        $this->load->model('Bkk_sekolah_model', 'bkk');
        $this->load->helper('email');

        // redirect to Login page if user not logged in
        $this->verify_pencari_kerja();
    }

    public function upload_image()
    {
        $config['upload_path']          = './assets/uploads/photo_profile/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 2000;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('photo_profile')) {
            $error = array('error' => $this->upload->display_errors());
            set_alert('danger', $error['error']);
            redirect(current_url());
        } else {
            return $this->upload->data();
        }
    }

    public function index()
    {
        $this->nav = 'pencari_kerja';
        $this->nav_child = 'pencari_kerja';

        $pencaker = $this->mUser;

        if (!empty($pencaker->email)) {
            $email = $pencaker->email;
        } else if (!empty($pencaker->google_email)) {
            $email = $pencaker->google_email;
        } else {
            $email = $pencaker->facebook_email;
        }

        $pencaker->email = $email;

        $this->mViewData['pencaker'] = $pencaker;
        $this->mViewData['dashboard_sidebar'] = 'dashboard';

        $this->render('pencari_kerja', 'user_layout');
    }

    public function edit_profile()
    {
        $this->nav = 'edit_profile';
        $this->nav_child = 'edit_profile';

        $pencaker = $this->mUser;

        if ($this->input->post('email')) {
            // get form request
            $profile_data = $this->input->post();

            // upload image validation
            if (!empty($_FILES['photo_profile']['name'])) {
                $photo_profile = $this->upload_image();
                $profile_data['photo_profile'] = 'assets/uploads/photo_profile/' . $photo_profile['file_name'];
            }

            // password validation
            if (!empty($pencaker->email)) {
                $password_data = elements(['current_password', 'new_password', 'retype_password'], $this->input->post());

                // jika current password tidak kosong
                if (!empty($password_data['current_password'])) {

                    // jika current password salah
                    if (!verify_pw($password_data['current_password'], $pencaker->password)) {
                        set_alert('danger', 'Password saat ini yang Anda masukan salah !!');
                        redirect('pencari_kerja/edit_profile');
                    } else {

                        // jika password baru krg dari 8 karakter
                        if (count($password_data['new_password']) < 8) {
                            set_alert('danger', 'Password Minimal 8 karakter !!');
                            redirect('pencari_kerja/edit_profile');
                        }

                        // jika new dan retype password berbeda
                        if ($password_data['new_password'] != $password_data['retype_password']) {
                            set_alert('danger', 'New password dan retype password tidak sama !!');
                            redirect('pencari_kerja/edit_profile');
                        }

                        // PASSWORD VALID
                        $profile_data['password'] = hash_pw($password_data['new_password']);
                    }
                }
            }

            // DATA VALID
            // unset unused data
            unset($profile_data['current_password']);
            unset($profile_data['new_password']);
            unset($profile_data['retype_password']);

            // payment process = update user payment
            $this->pencaker->update($pencaker->id, $profile_data);

            if (isset($profile_data['password'])) {
                set_alert('success', 'Data diri dan Password Anda berhasil diubah !!');
            } else {
                set_alert('success', 'Data diri Anda berhasil diubah !!');
            }

            redirect('pencari_kerja/edit_profile');
        }

        if (!empty($pencaker->email)) {
            $email = $pencaker->email;
        } else if (!empty($pencaker->google_email)) {
            $email = $pencaker->google_email;
        } else {
            $email = $pencaker->facebook_email;
        }

        $pencaker->email = $email;

        $this->mViewData['pencaker'] = $pencaker;
        $this->mViewData['dashboard_sidebar'] = 'edit_profile';

        $this->render('pencari_kerja/edit_profile', 'user_layout');
    }

    public function resume()
    {
        $this->nav = 'lamaran';
        $this->nav_child = 'lamaran';

        $pencaker = $this->mUser;

        $this->mViewData['pencaker'] = $pencaker;
        $this->mViewData['dashboard_sidebar'] = 'resume';

        $this->render('pencari_kerja/resume', 'user_layout');
    }

    public function edit_resume()
    {
        $this->nav = 'ubah_lamaran';
        $this->nav_child = 'ubah_lamaran';

        $pencaker = $this->mUser;

        $this->mViewData['pencaker'] = $pencaker;
        $this->mViewData['dashboard_sidebar'] = 'edit_resume';

        $this->render('pencari_kerja/edit_resume', 'user_layout');
    }
}
