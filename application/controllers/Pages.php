<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home page
 */
class Pages extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_nav($page_slug)
	{
		$current = 'home';

		$navbar_link = array(
			'about-gps' => array('achievements','credo-gps','gps-philosophy','history','mars-global-prestasi','vision-mission'),
			'international-program' => array('ip-curriculum','ip-students-tallent-performance',),
			'kindergarten' => array('global-prestasi-montessori','global-prestasi-kids'),
			'contact-us' => array('contact-us'),
			'vacancy' => array('vacancy'),
			'information' => array('admission','alumni-profile','recruitment-page'),
			);

		foreach ($navbar_link as $parent => $child)
		{
			if ( in_array($page_slug, $child) )
			{
				$current = $parent;
			}
		}

		$this->nav = $current;
		$this->nav_child = $page_slug;

	}

	public function index($page_slug = 'credo-gps')
	{
		$this->get_nav($page_slug);
		$this->mPageTitle = $page_slug;
		$this->mViewData['page'] = $this->global->get_data('page', ['page_slug' => $page_slug], TRUE);

		$galery_street_view_category = $this->global->get_data('galeri_category', array('galeri_category_slug' => 'google-street-view'), TRUE);

		$this->db->limit(4);
		$this->db->order_by('create_date', 'DESC');
		//$this->global->join_user_admin('galeri');
		$this->mViewData['galery_street_view'] = $this->global->get_data('galeri', array('galeri_category_id' => $galery_street_view_category->galeri_category_id));

		$all_in_one_pages = array('history', 'alumni', 'contact-us','vacancy');

		$selected_pages = in_array($page_slug, $all_in_one_pages) ? 'all_in_one' : $page_slug;

		//$curriculum = array('gp-elementary');
		$curriculum = array();

		$this->get_slider($page_slug);

		if ( in_array($page_slug, $curriculum) )
		{
			$selected_pages = 'curriculum';
		}
		
		

		$this->render('pages/'.$selected_pages, 'two');
	}
   //public function index($page_slug = 'vacancy')
//	{
	// $this->render('vacancy', 'two');  
	// 	$this->render('achievement', 'two');
//	}
	public function get_slider($page_slug)
	{
		$slider_category_array = array(
			'gp-shs' => 'slider-sma',
			'gp-jhs' => 'slider-smp',
			'gp-elementary' => 'slider-sd'
		);

		$slider_category = $slider_category_array[$page_slug];

		$galeri_category = $this->global->get_data('galeri_category', array('galeri_category_slug' => $slider_category), TRUE);


		$this->db->limit(15);
		$this->db->order_by('create_date', 'DESC');
		$this->mViewData['slider'] = $this->global->get_data('galeri', array('galeri_category_id' => $galeri_category->galeri_category_id));
	}

	public function blog_list($category_slug = '')
	{

		if ( empty($category_slug) )
		{
			$blog_filter = array('category_slug != ' => 'upcoming-event');
		}
		else
		{
			$blog_filter = array('category_slug' => $category_slug);
		}

		
		$uri = 'blog';
		$perpage = 4;

		$this->global->join_category();
		$all_blog = $this->global->get_data('blog', $blog_filter);
		$total_rows = count($all_blog);
		$paging_config = init_pagination($uri, $total_rows, $perpage);
		/*echo $total_rows;
		exit($total_rows);*/

		$page = $this->input->get('page') == null ? 0 : ( $this->input->get('page') - 1 ) * $perpage;

	//	$this->db->order_by('blog_id', 'DESC');
		$this->db->order_by('blog_date', 'DESC');
		$this->db->limit($paging_config['per_page'], $page);
		$this->global->join_category();
		$this->global->join_user_admin('blog');

		$this->mViewData['paging'] = $this->pagination->create_links();
		$this->mViewData['blog'] = $this->global->get_data('blog', $blog_filter);

		$this->render('pages/blog_list', 'two');
	}

	public function blog_detail($blog_slug)
	{

		$this->global->join_category();
		$this->global->join_user_admin('blog');
		$blog = $this->global->get_data('blog', array('blog_slug' => $blog_slug),TRUE);
		$this->mViewData['blog'] = $blog;

		$this->global->join_category();
		$this->global->join_user_admin('blog');
		$this->db->limit(2);
		//$this->db->order_by('blog_id', 'DESC');
			$this->db->order_by('blog_date', 'DESC');
		$this->mViewData['blog_related'] = $this->global->get_data('blog', array('blog.category_id' => $blog->category_id, 'blog_id != ' => $blog->blog_id));


		$page_name = $this->uri->segment(1);
		$blog_id = $blog->blog_id;

		$this->save_blog_info_by_user($blog_id, $page_name);

		$this->render('pages/blog_detail', 'two');
	}

	public function save_blog_info_by_user($blog_id, $page)
	{
		$info = array(
			'blog_info_id' => null,
			'blog_id' => $blog_id,
			'page' => $page,
			'ip_address' => $this->input->ip_address(),
			'mac_address' => null,
			'agent' => get_user_agent(),
			'date' => date('Y-m-d H:i:s')
			);

		$this->db->insert('blog_info_by_user', $info);
	}

	public function videos()
	{
		$video = $this->global->get_data('video');
		$this->mViewData['videos'] = $video;

		$this->render('videos', 'two');
	}
  
	public function images()
	{
		$this->global->join_galeri_category();
		$image = $this->global->get_data('galeri', array('galeri_category_slug' => 'google-street-view'));
		$this->mViewData['images'] = $image;

		$this->render('images', 'two');
	}

	public function achievement()
	{
		$this->global->join_galeri_category();
		$image = $this->global->get_data('galeri', array('galeri_category_slug' => 'achievement'));
		$this->mViewData['images'] = $image;

		$this->render('achievement', 'two');
	}
}
