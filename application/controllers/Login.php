<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Login page
 */
class Login extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Pencari_kerja_model', 'pencaker');
		$this->load->helper('email');
	}

	public function index()
	{
		$this->nav = 'login';
		$this->nav_child = 'login';

		$this->render('login', 'empty');

		if (validate_form()) {
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$pencaker = $this->pencaker->get_by([
				'email'		=> $email,
			]);

			if (!empty($pencaker)) {
				// "remember me"
				if ($this->input->post('remember')) {
					$this->session->sess_expire_on_close = FALSE;
					$this->session->sess_update();
				}

				// check password
				if (verify_pw($password, $pencaker->password)) {
					$pencaker = (array) $pencaker;
					// limited fields to store in session
					$fields = array('id', 'full_name', 'email', 'id_bkk_sekolah', 'payment_info', 'register_status', 'payment_status', 'member_status', 'created_at', 'updated_at');
					$pencaker_data = elements($fields, $pencaker);

					// save to user session
					login_user($pencaker_data);

					if ($pencaker['payment_status'] == 0) {
						//set_alert('danger', 'Akun Anda belum aktif, silahkan cek email untuk aktivasi.');
						redirect('register/join_member/' . $pencaker['activation_code'] . '/' . $pencaker['id'] . '/' . 'signin');
					} 

					if ($pencaker['member_status'] == 0) {
						//set_alert('danger', 'Akun Anda belum aktif, silahkan cek email untuk aktivasi.');
						redirect('register/payment_verification/' . $pencaker['payment_code']);
					}

					// success
					set_alert('success', 'Login success.');
					redirect('pencari_kerja');
					exit;
				}
			}

			// failed
			$this->session->set_flashdata('form_fields', ['email' => $email]);
			set_alert('danger', 'Oops!! data Anda tidak ditemukan, silahkan register terlebih dahulu.');
			redirect('login');
		}
	}

	public function destroy() {
		logout_user();
		set_alert('success', 'Anda berhasil logout');
		redirect('login');
	}
}
