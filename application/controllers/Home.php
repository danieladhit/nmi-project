<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home page
 */
class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Pencari_kerja_model', 'pencaker');
		$this->load->model('Bkk_sekolah_model', 'bkk');
		$this->load->helper('email');
	}

	public function index()
	{
		$this->nav = 'home';
		$this->nav_child = 'home';
		
		$bkk = $this->bkk->get_all();
		$this->mViewData['bkk'] = $bkk;
		$this->render('home', 'with_home_header');
		
	}
	
}
