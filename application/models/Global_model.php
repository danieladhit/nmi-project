<?php 

class Global_model extends MY_Model {

	//------------------------ single and many result template --------

	private function many_result($values)
	{
		if ( empty($values) )
		{
			$result = $this->get_all();
		}
		else
		{
			$result = $this->get_many_by($values);
		}
		
		return $result;
	}

	private function single_result($value)
	{
		$row = $this->get_by($value);
		return $row;
	}

	private function render_result($values, $single)
	{
		if ( $single )
		{
			return $this->single_result($values);
		}
		else 
		{
			return $this->many_result($values);
		}
	}

	public function get_data($table = 'your table name', $values = NULL, $single = FALSE)
	{
		$this->_table = $table;
		return $this->render_result($values, $single);
	}

	public function count_data($table = 'your table name', $values = NULL)
	{
		$this->_table = $table;

		if ( empty($values) )
		{
			$result = $this->count_all();
		}
		else
		{
			$result = $this->count_by($values);
		}
		
		return $result;
	}

	//------------------------ ./single and many result template --------

	public function join_category()
	{
		$this->db->join('category', 'category.category_id = blog.category_id');
	}

	public function join_galeri_category()
	{
		$this->db->join('galeri_category', 'galeri_category.galeri_category_id = galeri.galeri_category_id');
	}

	public function join_user_admin($join_table)
	{
		$this->db->join("user","user.user_id = $join_table.user_id");
	}

}