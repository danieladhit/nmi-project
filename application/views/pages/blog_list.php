<div class="post_section clearfix">
	<div class="container">
		<div class="row">

			<div class="col-xs-12 col-sm-8 post_left">
				<div class="post_left_section post_left_border">
					<?php foreach($blog as $key => $row){ ?>
					<div class="post">
						<div class="post_thumb">
							<img src="<?=base_url('uploads/blog/'.$row->blog_picture) ?>" alt="">
						</div><!--end post thumb-->
						<div class="meta">
							<span class="author">By: <a href="<?=base_url('blog/'.$row->blog_slug) ?>"><?=$row->username ?></a></span>
							<span class="category"> <a href="<?=base_url('blog/'.$row->blog_slug) ?>"><?=$row->category_title ?></a></span>
							<span class="date">Posted: <a href="<?=base_url('blog/'.$row->blog_slug) ?>"><?=$row->blog_date; ?></a></span>
							<span class="date">Posted: <a href="<?=base_url('blog/'.$row->blog_slug) ?>"><?=date('F d, Y', strtotime($row->create_date)) ?></a></span>-->
						</div><!--end meta-->
						<h1><a href="<?=base_url('blog/'.$row->blog_slug) ?>"><?=character_limiter($row->blog_title, 100) ?></a></h1>
						<div class="post_desc">
							<p><?=character_limiter(strip_tags($row->blog_description), 300) ?></p>
						</div><!--end post desc-->
						<!-- <div class="post_bottom">
							<ul>
								<li class="like">
									<a href="#">
										<img src="src/themes/two/img/news/like_icon.png" alt="">
										<span>12</span>
									</a>
								</li>
								<li class="share">
									<a href="#">
										<img src="src/themes/two/img/news/share_icon.png" alt="">
										<span>12</span>
									</a>
								</li>
								<li class="favorite">
									<a href="#">
										<img src="src/themes/two/img/news/favorite_icon.png" alt="">
										<span>12</span>
									</a>
								</li>
							</ul>
						</div> --><!--end post bottom-->
					</div><!--end post-->
					<?php } ?>

					<?php print_r($paging) ?>

				</div><!--end post left section-->
			</div><!--end post_left-->

			<div class="col-xs-12 col-sm-4 post_right">
				
				<?php include "sidebar.php"; ?>

			</div><!--end post_right-->

		</div>
	</div>
</div>