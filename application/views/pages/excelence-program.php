<div class="projects-horizontal" style="background: url(src/images/bg-idea.jpg) no-repeat;">
    <div class="container">
        <div class="intro">
            <h1 class="text-center cr-blue" style="margin-top: 40px;">Excellence Program </h1>
        </div>
        <div class="row projects">
            <div class="col-sm-6 item">
                <div class="row">
                    <div class="col-md-5">
                        <a href="#"><img src="src/images/i-idea-image.png" class="img-circle img-responsive" /></a>
                    </div>
                    <div class="col-md-7">
                        <h3 class="name cr-blue"><span class="cr-red" style="font-size: 120%;">I</span>nformation Technology</h3>
                        <p class="description cr-black"><strong>ICT - Based Learning</strong></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 item">
                <div class="row">
                    <div class="col-md-5">
                        <a href="#"><img src="src/images/d-idea-image.png" class="img-circle img-responsive" /></a>
                    </div>
                    <div class="col-md-7">
                        <h3 class="name cr-blue"><span class="cr-red" style="font-size: 120%;">D</span>iversity</h3>
                        <p class="description cr-black"><strong>Harmony In Diversity</strong></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 item">
                <div class="row">
                    <div class="col-md-5">
                        <a href="#"><img src="src/images/e-idea-image.png" class="img-circle img-responsive" /></a>
                    </div>
                    <div class="col-md-7">
                        <h3 class="name cr-blue"><span class="cr-red" style="font-size: 120%;">E</span>nglish</h3>
                        <p class="description cr-black"><strong>English Environment</strong></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 item">
                <div class="row">
                    <div class="col-md-5">
                        <a href="#"><img src="src/images/a-idea-image.png" class="img-circle img-responsive" /></a>
                    </div>
                    <div class="col-md-7">
                        <h3 class="name cr-blue"><span class="cr-red" style="font-size: 120%;">A</span>chievement</h3>
                        <p class="description cr-black"><strong>Academic, non academic national and International level</strong></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>