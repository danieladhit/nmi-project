<div class="custom_content custom">
	<div class="container">
		<div class="row">

			<div class="col-xs-12 col-sm-8 custom_right">
				<div class="single_content_left default-list-style">
					<?php $this->load->view('_partials/breadcrumb'); ?>
					<h3><?=$page->page_title ?></h3>

					<?php if( !empty($page->page_picture)) { ?>
					<img width="100%" class="img-responsive" src="<?=upload_url('page/'.$page->page_picture) ?>" alt="image">
					<?php } ?>

					<?=$page->page_description ?>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-4 custom_left">
				<?php include "sidebar.php"; ?>
			</div>
		</div><!--end row-->
	</div>
</div>

<script>

	$(function(){
		//$(".custom-list-box ul, .custom-list-box ol").css('list_style' : 'none');
		//$(".custom-list-box ul li, .custom-list-box ol li").append("<i class='fa fa-check-circle-o'></i>");

		$("table").addClass('table table-striped table-hover').attr('border',0);
	});
	
</script>