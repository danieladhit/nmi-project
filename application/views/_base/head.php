<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<base href="<?php echo $base_url; ?>" />

	<title><?php echo $page_title; ?></title>

	<meta name="format-detection" content="telephone=no">
	<meta name="apple-mobile-web-app-capable" content="yes">

	<meta name="description" content="description content here">
	<meta name="keywords" content="keywords content here">

	<meta itemprop="name" content="<?php echo $page_title; ?> | name content here">
	<meta itemprop="description" content="name content here">
	<meta itemprop="image" content="">


	<!-- Favicon -->
    <link rel="icon" href="assets/logo/white/2.png">
    <!-- <link rel="apple-touch-icon" href="themes/oficiona/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="themes/oficiona/images/icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="themes/oficiona/images/icon-114x114.png"> -->

	<?php
	foreach ($meta_data as $name => $content) {
		if (!empty($content))
			echo "<meta name='$name' content='$content'>" . PHP_EOL;
	}

	foreach ($stylesheets as $media => $files) {
		foreach ($files as $file) {
			$url = starts_with($file, 'http') ? $file : base_url($file);
			echo "<link href='$url' rel='stylesheet' media='$media'>" . PHP_EOL;
		}
	}

	foreach ($scripts['head'] as $file) {
		$url = starts_with($file, 'http') ? $file : base_url($file);
		echo "<script src='$url'></script>" . PHP_EOL;
	}
	?>


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->


</head>

<body>
