
    <!-- Jobs -->
    <div class="section-padding-bottom alice-bg">
      <div class="container">
        <div class="row">
          <div class="col">
            <!-- <ul class="nav nav-tabs job-tab" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="recent-tab" data-toggle="tab" href="#recent" role="tab" aria-controls="recent" aria-selected="true">Recent Job</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="feature-tab" data-toggle="tab" href="#feature" role="tab" aria-controls="feature" aria-selected="false">Feature Job</a>
              </li>
            </ul> -->
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="recent" role="tabpanel" aria-labelledby="recent-tab">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="javascript:void(0)">
                          <img src="assets/images/perusahaan/honda_lock.jpg" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a class="work-on-progress" href="javascript:void(0)">Operator Produksi - Honda Lock </a></h4>
                          <div class="info">
                            <span class="company"><a href="javascript:void(0)"><i data-feather="briefcase"></i>Kontrak</a></span>
                            <span class="office-location"><a href="javascript:void(0)"><i data-feather="map-pin"></i>Jakarta</a></span>
                            <span class="job-type full-time"><a href="javascript:void(0)"><i data-feather="clock"></i>Full Time</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="javascript:void(0)" class="button">Apply Now</a>
                            <a href="javascript:void(0)" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="javascript:void(0)">
                          <img src="assets/images/perusahaan/prakarsa_alam_segar.jpeg" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a class="work-on-progress" href="javascript:void(0)">Operator Produksi - Prakarsa Alam Segar</a></h4>
                          <div class="info">
                            <span class="company"><a href="javascript:void(0)"><i data-feather="briefcase"></i>Kontrak</a></span>
                            <span class="office-location"><a href="javascript:void(0)"><i data-feather="map-pin"></i>Jakarta</a></span>
                            <span class="job-type full-time"><a href="javascript:void(0)"><i data-feather="clock"></i>Full Time</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="javascript:void(0)" class="button">Apply Now</a>
                            <a href="javascript:void(0)" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="javascript:void(0)">
                          <img src="assets/images/perusahaan/multi_karya.jpg" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a class="work-on-progress" href="javascript:void(0)">Operator Produksi - Multi Karya</a></h4>
                          <div class="info">
                            <span class="company"><a href="javascript:void(0)"><i data-feather="briefcase"></i>Kontrak</a></span>
                            <span class="office-location"><a href="javascript:void(0)"><i data-feather="map-pin"></i>Jakarta</a></span>
                            <span class="job-type full-time"><a href="javascript:void(0)"><i data-feather="clock"></i>Full Time</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="javascript:void(0)" class="button">Apply Now</a>
                            <a href="javascript:void(0)" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="javascript:void(0)">
                          <img src="assets/images/perusahaan/soho.png" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a class="work-on-progress" href="javascript:void(0)">Operator Produksi - Soho</a></h4>
                          <div class="info">
                            <span class="company"><a href="javascript:void(0)"><i data-feather="briefcase"></i>Kontrak</a></span>
                            <span class="office-location"><a href="javascript:void(0)"><i data-feather="map-pin"></i>Jakarta</a></span>
                            <span class="job-type full-time"><a href="javascript:void(0)"><i data-feather="clock"></i>Full Time</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="javascript:void(0)" class="button">Apply Now</a>
                            <a href="javascript:void(0)" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- <div class="tab-pane fade" id="feature" role="tabpanel" aria-labelledby="feature-tab">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="#">
                          <img src="https://themerail.com/html/oficiona/images/job/company-logo-10.png" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a href="job-listing.html">UI Designer</a></h4>
                          <div class="info">
                            <span class="company"><a href="#"><i data-feather="briefcase"></i>Degoin</a></span>
                            <span class="office-location"><a href="#"><i data-feather="map-pin"></i>San Francisco</a></span>
                            <span class="job-type part-time"><a href="#"><i data-feather="clock"></i>Part Time</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="#" class="button">Apply Now</a>
                            <a href="#" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="#">
                          <img src="https://themerail.com/html/oficiona/images/job/company-logo-1.png" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a href="job-listing.html">Designer Required</a></h4>
                          <div class="info">
                            <span class="company"><a href="#"><i data-feather="briefcase"></i>Theoreo</a></span>
                            <span class="office-location"><a href="#"><i data-feather="map-pin"></i>New York City</a></span>
                            <span class="job-type full-time"><a href="#"><i data-feather="clock"></i>Full Time</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="#" class="button">Apply Now</a>
                            <a href="#" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="#">
                          <img src="https://themerail.com/html/oficiona/images/job/company-logo-2.png" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a href="job-listing.html">Project Manager</a></h4>
                          <div class="info">
                            <span class="company"><a href="#"><i data-feather="briefcase"></i>Degoin</a></span>
                            <span class="office-location"><a href="#"><i data-feather="map-pin"></i>San Francisco</a></span>
                            <span class="job-type part-time"><a href="#"><i data-feather="clock"></i>Part Time</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="#" class="button">Apply Now</a>
                            <a href="#" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="#">
                          <img src="https://themerail.com/html/oficiona/images/job/company-logo-1.png" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a href="job-listing.html">Designer Required</a></h4>
                          <div class="info">
                            <span class="company"><a href="#"><i data-feather="briefcase"></i>Theoreo</a></span>
                            <span class="office-location"><a href="#"><i data-feather="map-pin"></i>New York City</a></span>
                            <span class="job-type full-time"><a href="#"><i data-feather="clock"></i>Full Time</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="#" class="button">Apply Now</a>
                            <a href="#" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="#">
                          <img src="https://themerail.com/html/oficiona/images/job/company-logo-8.png" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a href="job-listing.html">Restaurant Team Member - Crew </a></h4>
                          <div class="info">
                            <span class="company"><a href="#"><i data-feather="briefcase"></i>Geologitic</a></span>
                            <span class="office-location"><a href="#"><i data-feather="map-pin"></i>New Orleans</a></span>
                            <span class="job-type temporary"><a href="#"><i data-feather="clock"></i>Temporary</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="#" class="button">Apply Now</a>
                            <a href="#" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="#">
                          <img src="https://themerail.com/html/oficiona/images/job/company-logo-9.png" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a href="job-listing.html">Nutrition Advisor</a></h4>
                          <div class="info">
                            <span class="company"><a href="#"><i data-feather="briefcase"></i>Theoreo</a></span>
                            <span class="office-location"><a href="#"><i data-feather="map-pin"></i>New York City</a></span>
                            <span class="job-type full-time"><a href="#"><i data-feather="clock"></i>Full Time</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="#" class="button">Apply Now</a>
                            <a href="#" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="#">
                          <img src="https://themerail.com/html/oficiona/images/job/company-logo-3.png" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a href="job-details.html">Land Development Marketer</a></h4>
                          <div class="info">
                            <span class="company"><a href="#"><i data-feather="briefcase"></i>Realouse</a></span>
                            <span class="office-location"><a href="#"><i data-feather="map-pin"></i>Washington, D.C.</a></span>
                            <span class="job-type freelance"><a href="#"><i data-feather="clock"></i>Freelance</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="#" class="button">Apply Now</a>
                            <a href="#" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="#">
                          <img src="https://themerail.com/html/oficiona/images/job/company-logo-2.png" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a href="job-details.html">Project Manager</a></h4>
                          <div class="info">
                            <span class="company"><a href="#"><i data-feather="briefcase"></i>Degoin</a></span>
                            <span class="office-location"><a href="#"><i data-feather="map-pin"></i>San Francisco</a></span>
                            <span class="job-type part-time"><a href="#"><i data-feather="clock"></i>Part Time</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="#" class="button">Apply Now</a>
                            <a href="#" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="#">
                          <img src="https://themerail.com/html/oficiona/images/job/company-logo-8.png" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a href="job-details.html">Restaurant Team Member - Crew </a></h4>
                          <div class="info">
                            <span class="company"><a href="#"><i data-feather="briefcase"></i>Geologitic</a></span>
                            <span class="office-location"><a href="#"><i data-feather="map-pin"></i>New Orleans</a></span>
                            <span class="job-type temporary"><a href="#"><i data-feather="clock"></i>Temporary</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="#" class="button">Apply Now</a>
                            <a href="#" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="#">
                          <img src="https://themerail.com/html/oficiona/images/job/company-logo-9.png" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a href="job-details.html">Nutrition Advisor</a></h4>
                          <div class="info">
                            <span class="company"><a href="#"><i data-feather="briefcase"></i>Theoreo</a></span>
                            <span class="office-location"><a href="#"><i data-feather="map-pin"></i>New York City</a></span>
                            <span class="job-type full-time"><a href="#"><i data-feather="clock"></i>Full Time</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="#" class="button">Apply Now</a>
                            <a href="#" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="#">
                          <img src="https://themerail.com/html/oficiona/images/job/company-logo-10.png" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a href="job-details.html">UI Designer</a></h4>
                          <div class="info">
                            <span class="company"><a href="#"><i data-feather="briefcase"></i>Degoin</a></span>
                            <span class="office-location"><a href="#"><i data-feather="map-pin"></i>San Francisco</a></span>
                            <span class="job-type part-time"><a href="#"><i data-feather="clock"></i>Part Time</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="#" class="button">Apply Now</a>
                            <a href="#" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                    <div class="job-list half-grid">
                      <div class="thumb">
                        <a href="#">
                          <img src="https://themerail.com/html/oficiona/images/job/company-logo-3.png" class="img-fluid" alt="">
                        </a>
                      </div>
                      <div class="body">
                        <div class="content">
                          <h4><a href="job-details.html">Land Development Marketer</a></h4>
                          <div class="info">
                            <span class="company"><a href="#"><i data-feather="briefcase"></i>Realouse</a></span>
                            <span class="office-location"><a href="#"><i data-feather="map-pin"></i>Washington, D.C.</a></span>
                            <span class="job-type freelance"><a href="#"><i data-feather="clock"></i>Freelance</a></span>
                          </div>
                        </div>
                        <div class="more">
                          <div class="buttons">
                            <a href="#" class="button">Apply Now</a>
                            <a href="#" class="favourite"><i data-feather="heart"></i></a>
                          </div>
                          <p class="deadline">Deadline: Oct 31, 2018</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Jobs End -->

    <!-- Top Companies -->
    <div class="section-padding-top padding-bottom-90">
      <div class="container">
        <div class="row">
          <div class="col">
            <div class="section-header">
              <h2>Top Companies</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="company-carousel owl-carousel">
              <div class="company-wrap">
                <div class="thumb">
                  <a class="work-on-progress" href="javascript:void(0)">
                    <img style="height: 70px; width: auto" src="assets/images/perusahaan/alfamart.png" class="img-fluid" alt="">
                  </a>
                </div>
                <div class="body">
                  <h4><a href="employer-details.html">Sumber Alfaria</a></h4>
                  <span>Jakarta, Indonesia</span>
                  <a class="work-on-progress" href="javascript:void(0)" class="button">6 Posisi</a>
                </div>
              </div>
              <div class="company-wrap">
                <div class="thumb">
                  <a class="work-on-progress" href="javascript:void(0)">
                    <img style="height: 70px; width: auto" src="assets/images/perusahaan/jjsm.png" class="img-fluid" alt="">
                  </a>
                </div>
                <div class="body">
                  <h4><a href="employer-details.html">JJ Surya Makmur</a></h4>
                  <span>Subang, Indonesia</span>
                  <a class="work-on-progress" href="javascript:void(0)" class="button">2 Posisi</a>
                </div>
              </div>
              <div class="company-wrap">
                <div class="thumb">
                  <a class="work-on-progress" href="javascript:void(0)">
                    <img style="height: 70px; width: auto" src="assets/images/perusahaan/champ.png" class="img-fluid" alt="">
                  </a>
                </div>
                <div class="body">
                  <h4><a href="employer-details.html">Champ Resto</a></h4>
                  <span>Bandung, Indonesia</span>
                  <a class="work-on-progress" href="javascript:void(0)" class="button">4 Posisi</a>
                </div>
              </div>
              <div class="company-wrap">
                <div class="thumb">
                  <a class="work-on-progress" href="javascript:void(0)">
                    <img style="height: 70px; width: auto" src="assets/images/perusahaan/dayacipta.png" class="img-fluid" alt="">
                  </a>
                </div>
                <div class="body">
                  <h4><a href="employer-details.html">Daya Cipta</a></h4>
                  <span>Karawang, Indonesia</span>
                  <a class="work-on-progress" href="javascript:void(0)" class="button">1 Posisi</a>
                </div>
              </div>
              <div class="company-wrap">
                <div class="thumb">
                  <a class="work-on-progress" href="javascript:void(0)">
                    <img style="height: 70px; width: auto" src="assets/images/perusahaan/djabesmen.png" class="img-fluid" alt="">
                  </a>
                </div>
                <div class="body">
                  <h4><a href="employer-details.html">Djabesmen</a></h4>
                  <span>Jakarta, Indonesia</span>
                  <a class="work-on-progress" href="javascript:void(0)" class="button">4 Posisi</a>
                </div>
              </div>
              <div class="company-wrap">
                <div class="thumb">
                  <a class="work-on-progress" href="javascript:void(0)">
                    <img style="height: 70px; width: auto" src="assets/images/perusahaan/dwa.png" class="img-fluid" alt="">
                  </a>
                </div>
                <div class="body">
                  <h4><a href="employer-details.html">Dasa Windu Agung</a></h4>
                  <span>Jakarta, Indonesia</span>
                  <a class="work-on-progress" href="javascript:void(0)" class="button">2 Posisi</a>
                </div>
              </div>
              <div class="company-wrap">
                <div class="thumb">
                  <a class="work-on-progress" href="javascript:void(0)">
                    <img style="height: 70px; width: auto" src="assets/images/perusahaan/stanley.png" class="img-fluid" alt="">
                  </a>
                </div>
                <div class="body">
                  <h4><a href="employer-details.html">Stanley Electric</a></h4>
                  <span>Tangerang, Indonesia</span>
                  <a class="work-on-progress" href="javascript:void(0)" class="button">3 Posisi</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Top Companies End -->
    
    <!-- Fun Facts -->
    <div class="padding-top-90 padding-bottom-60 fact-bg">
      <div class="container">
        <div class="row fact-items">
          <div class="col-md-3 col-sm-6">
            <div class="fact">
              <div class="fact-icon">
                <i data-feather="briefcase"></i>
              </div>
              <p class="fact-number"><span class="count" data-form="0" data-to="156"></span></p>
              <p class="fact-name">Lowongan Kerja</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="fact">
              <div class="fact-icon">
                <i data-feather="users"></i>
              </div>
              <p class="fact-number"><span class="count" data-form="0" data-to="523"></span></p>
              <p class="fact-name">Pencari Kerja</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="fact">
              <div class="fact-icon">
                <i data-feather="file-text"></i>
              </div>
              <p class="fact-number"><span class="count" data-form="0" data-to="523"></span></p>
              <p class="fact-name">Lamaran</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="fact">
              <div class="fact-icon">
                <i data-feather="award"></i>
              </div>
              <p class="fact-number"><span class="count" data-form="0" data-to="75"></span></p>
              <p class="fact-name">Perusahaan</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Fun Facts End -->

    <!-- Registration Box -->
    <div class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="call-to-action-box candidate-box">
              <div class="icon">
                <img src="https://themerail.com/html/oficiona/images/register-box/1.png" alt="">
              </div>
              <span>Apakah Anda</span>
              <h3>Pencari Kerja?</h3>
              <a href="register" data-toggle="modal" data-target="#exampleModalLong2">Register Now <i class="fas fa-arrow-right"></i></a>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="call-to-action-box employer-register">
              <div class="icon">
                <img src="https://themerail.com/html/oficiona/images/register-box/2.png" alt="">
              </div>
              <span>Apakah Anda</span>
              <h3>Perusahaan?</h3>
              <a class="work-on-progress" href="javascript:void(0)">Register Now <i class="fas fa-arrow-right"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Registration Box End -->

    <!-- Modal -->
    <div class="account-entry">
      <div class="modal fade" id="exampleModalLong3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><i data-feather="edit"></i>Registration</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="account-type">
                <a href="#" class="candidate-acc"><i data-feather="user"></i>Candidate</a>
                <a href="#" class="employer-acc active"><i data-feather="briefcase"></i>Employer</a>
              </div>
              <form action="#">
                <div class="form-group">
                  <input type="text" placeholder="Username" class="form-control">
                </div>
                <div class="form-group">
                  <input type="email" placeholder="Email Address" class="form-control">
                </div>
                <div class="form-group">
                  <input type="password" placeholder="Password" class="form-control">
                </div>
                <div class="more-option terms">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck3">
                    <label class="form-check-label" for="defaultCheck3">
                      I accept the <a href="#">terms & conditions</a>
                    </label>
                  </div>
                </div>
                <button class="button primary-bg btn-block">Register</button>
              </form>
              <div class="shortcut-login">
                <span>Or connect with</span>
                <div class="buttons">
                  <a href="#" class="facebook"><i class="fab fa-facebook-f"></i>Facebook</a>
                  <a href="#" class="google"><i class="fab fa-google"></i>Google</a>
                </div>
                <p>Already have an account? <a href="#">Login</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Testimonial -->
    <div class="section-padding-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="testimonial">
              <div class="testimonial-quote">
                <img src="https://themerail.com/html/oficiona/images/testimonial/quote.png" class="img-fluid" alt="">
              </div>
              <div class="testimonial-for">
                <div class="testimonial-item">
                  <p>“On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charmsto send our denim shorts wardrob One”</p>
                  <h5>Maria Marlin @ Google</h5>
                </div>
                <div class="testimonial-item">
                  <p>“On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charmsto send our denim shorts wardrob Two”</p>
                  <h5>Laura Harper @ Amazon</h5>
                </div>
                <div class="testimonial-item">
                  <p>“On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charmsto send our denim shorts wardrob Three”</p>
                  <h5>John Doe @ Envato</h5>
                </div>
                <div class="testimonial-item">
                  <p>“On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charmsto send our denim shorts wardrob Four”</p>
                  <h5>Jenny Doe @ Dribbble</h5>
                </div>
                <div class="testimonial-item">
                  <p>“On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charmsto send our denim shorts wardrob Five”</p>
                  <h5>Michle Clark @ Apple</h5>
                </div>
                <div class="testimonial-item">
                  <p>“On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charmsto send our denim shorts wardrob Two”</p>
                  <h5>Laura Harper @ Amazon</h5>
                </div>
                <div class="testimonial-item">
                  <p>“On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charmsto send our denim shorts wardrob Three”</p>
                  <h5>John Doe @ Envato</h5>
                </div>
              </div>
              <div class="testimonial-nav">
                <div class="commenter-thumb">
                  <img src="https://themerail.com/html/oficiona/images/testimonial/thumb-1.jpg" class="img-fluid commenter" alt="">
                  <img src="https://themerail.com/html/oficiona/images/testimonial/1.png" class="comapnyLogo" alt="">
                </div>
                <div class="commenter-thumb">
                  <img src="https://themerail.com/html/oficiona/images/testimonial/thumb-2.jpg" class="img-fluid commenter" alt="">
                  <img src="https://themerail.com/html/oficiona/images/testimonial/2.png" class="comapnyLogo" alt="">
                </div>
                <div class="commenter-thumb">
                  <img src="https://themerail.com/html/oficiona/images/testimonial/thumb-3.jpg" class="img-fluid commenter" alt="">
                  <img src="https://themerail.com/html/oficiona/images/testimonial/3.png" class="comapnyLogo" alt="">
                </div>
                <div class="commenter-thumb">
                  <img src="https://themerail.com/html/oficiona/images/testimonial/thumb-4.jpg" class="img-fluid commenter" alt="">
                  <img src="https://themerail.com/html/oficiona/images/testimonial/4.png" class="comapnyLogo" alt="">
                </div>
                <div class="commenter-thumb">
                  <img src="https://themerail.com/html/oficiona/images/testimonial/thumb-5.jpg" class="img-fluid commenter" alt="">
                  <img src="https://themerail.com/html/oficiona/images/testimonial/5.png" class="comapnyLogo" alt="">
                </div>
                <div class="commenter-thumb">
                  <img src="https://themerail.com/html/oficiona/images/testimonial/thumb-2.jpg" class="img-fluid commenter" alt="">
                  <img src="https://themerail.com/html/oficiona/images/testimonial/2.png" class="comapnyLogo" alt="">
                </div>
                <div class="commenter-thumb">
                  <img src="https://themerail.com/html/oficiona/images/testimonial/thumb-3.jpg" class="img-fluid commenter" alt="">
                  <img src="https://themerail.com/html/oficiona/images/testimonial/3.png" class="comapnyLogo" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Testimonial End -->

