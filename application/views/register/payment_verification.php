<header class="header-2 access-page-nav">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="header-top">
                    <div class="logo-area">
                        <a href="home"><img src="assets/logo/4.png" alt="" style="height: 50px; width: auto;"></a>
                    </div>
                    <div class="top-nav">
                        <a href="home" class="account-page-link">Kembali ke halaman awal</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Breadcrumb -->
<div class="alice-bg padding-top-40 padding-bottom-40">
   
</div>
<!-- Breadcrumb End -->

<div class="alice-bg section-padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="block-wrapper">
                    <div class="payment-result">
                        <div class="icon">
                            <i data-feather="check"></i>
                        </div>
                        <h3>Tunggu yaa <span><?=$pencaker->full_name ?></span>, akun kamu sedang di <span>Verifikasi</span> oleh sistem kami</h3>
                        <p>Kami akan mengirim email jika proses sudah selesai, Terima kasih</p>
                        <div class="result">
                            <span>Kode Transaksi : <?=$payment_code ?></span>
                        </div>
                        <div class="w-100"></div>
                        <p>Silahkan hubungi Admin kami jika ada kendala, mohon hubungi Admin kami dengan menyertakan kode transaksi diatas.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>