<header class="header-2 access-page-nav">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="header-top">
                    <div class="logo-area">
                        <a href="home"><img src="assets/logo/4.png" alt="" style="height: 50px; width: auto;"></a>
                    </div>
                    <div class="top-nav">
                        <a href="home" class="account-page-link">Kembali ke halaman awal</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Breadcrumb -->
<div class="alice-bg padding-top-40 padding-bottom-40">

</div>
<!-- Breadcrumb End -->

<div class="alice-bg section-padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="block-wrapper" style="padding-bottom: 20px;">
                    <div class="payment-result">
                        <div class="icon">
                            <i data-feather="check"></i>
                        </div>

                        <?php if ($process == 'signup') { ?>
                            <h3>Hai <span><?= $pencaker->full_name ?></span>, kamu berhasil <span>registrasi</span></h3>
                        <?php } else { ?>
                            <h3>Hai <span><?= $pencaker->full_name ?></span>, kamu berhasil <span>login</span></h3>
                        <?php } ?>

                        <p>Silahkan melakukan pembayaran untuk masuk ke halaman Member.</p>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="block-wrapper">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="checkout-form">
                                <form id="formPayment" action="" method="POST" enctype="multipart/form-data">
                                    <div class="row">
                                        <label class="col-md-2 col-form-label"></label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="package-wrapper">
                                                        <div class="package professional">
                                                            <div class="package-header">
                                                                <h5 class="cost monthly-rate">Rp. 25.000<span>/Selamanya</span></h5>
                                                                <span class="title">Member</span>
                                                            </div>
                                                            <div class="package-features">
                                                                <ul>
                                                                    <li class="active">Akses Berbagai Fitur Member Selamanya</li>
                                                                    <li class="active">Notifikasi Lowongan Kerja Terbaru</li>
                                                                    <li class="active">Layanan E-Psikotest</li>
                                                                    <li class="active">Dashboard</li>
                                                                    <li class="active">Management Profile dan CV</li>
                                                                </ul>
                                                                <!-- <a href="#" class="button">Buy Now</a> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="w-100"></div>
                                                <div class="col">
                                                    <div class="padding-top-100"></div>
                                                    <p>Mohon melakukan pembayaran dengan nominal biaya sesuai total biaya yang tertera pada biaya layanan, jika sudah silahkan lakukan konfirmasi pembayaran dibawah ini :</p>

                                                    <?php echo alert_box(); ?>

                                                    <div class="payment-method" style="padding-top: 20px;">
                                                        <a href="#" data-method="bank_transfer" class="credit active"><i class="fas fa-credit-card"></i>Transfer Bank</a>
                                                        <a href="#" data-method="ovo_transfer" class="paypal"><i class="fas fa-barcode"></i>Transfer OVO</a>
                                                        <script>
                                                            $(".payment-method a").click(function() {
                                                                var paymentMethod = $(this).data('method');
                                                                var showMessage = "Anda memilih metode pembayaran melalui ";

                                                                if (paymentMethod == "bank_transfer") {
                                                                    alert(showMessage + "Transfer Bank");
                                                                } else {
                                                                    alert(showMessage + "Transfer OVO");
                                                                }

                                                                $("#paymentMethod").val(paymentMethod);
                                                            });
                                                        </script>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10 offset-md-2">
                                            <div class="row">
                                                <input type="hidden" value="bank_transfer" name="payment_method" id="paymentMethod">
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <div class="download-resume">
                                                            <div class="update-file">
                                                                <input required name="payment_file" id="paymentFile" type="file">Upload Bukti Pembayaran <i data-feather="edit-2"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label><i>*foto struk atm/screenshot pembayaran melalui aplikasi</i></label>
                                                    </div>
                                                </div>
                                                <div class="w-100"></div>
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <input required name="payment_number" id="paymentNumber" type="text" class="form-control" placeholder="No. Rekening / No. Terdaftar OVO">
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label><i>*silahkan isi nmr identitas berdasarkan metode pembayaran yang dipilih Transfer Bank/Transfer OVO</i></label>
                                                    </div>
                                                </div>
                                                <div class="w-100"></div>
                                                <div class="col">
                                                    <div class="form-group terms">
                                                        <input class="custom-radio" type="checkbox" id="radio-4" name="termsandcondition" required checked>
                                                        <label for="radio-4">
                                                            <span class="dot"></span> Anda menyetujui untuk menjadi member di sistem kami</a>
                                                        </label>
                                                    </div>
                                                    <button type="submit" class="button">Kirim Bukti Pembayaran</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="payment-summery">
                                <h4>Biaya Layanan</h4>
                                <ul>
                                    <li class="payment-list">
                                        <span class="title">Biaya member</span> <span class="amount">Rp. <?= number_format($price) ?></span>
                                    </li>
                                    <li class="payment-list">
                                        <span class="title">Kode Unik</span> <span class="amount">Rp. <?= number_format($payment_unique_id) ?></span>
                                    </li>
                                </ul>
                                <div class="total-amount">
                                    <span class="title">Total Biaya</span><span class="amount" style="font-weight: 600"><?= number_format($total_payment) ?></span>
                                </div>
                                <div class="total-amount">
                                    <span class="title"></span><button data-clipboard-text="<?= $total_payment ?>" class="copy-clipboard">Copy Total Biaya</button>
                                </div>
                                <br>
                                <div>
                                    <i>*Nominal pembayaran dengan kode unik dilakukan agar pembayaran Anda dapat segera dilacak dan diverifikasi oleh sistem kami.</i>
                                </div>
                            </div>

                            <br>

                            <div class="job-list half-grid">
                                <div class="thumb">
                                    <a href="#">
                                        <img src="assets/images/logo_bca.png" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="body">
                                    <div class="content">
                                        <h4><a href="job-details.html">Bank BCA</a></h4>
                                        <div class="info">
                                            <span class="company"><a href="#"><i data-feather="check-circle"></i>8415471641</a></span>
                                            <span class="office-location"><a href="#"><i data-feather="book"></i>a.n Dicky Yanuar Sianipar</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="job-list half-grid">
                                <div class="thumb">
                                    <a href="#">
                                        <img src="assets/images/logo_ovo.png" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="body">
                                    <div class="content">
                                        <h4><a href="job-details.html">OVO</a></h4>
                                        <div class="info">
                                            <span class="company"><a href="#"><i data-feather="check-circle"></i>081219364547</a></span>
                                            <span class="office-location"><a href="#"><i data-feather="book"></i>a.n Ronaldo Viktor Sianipar</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#formPayment").submit(function(e) {
        var paymentMethod = $("#paymentMethod").val();
        var paymentNumber = $("#paymentNumber").val();
        var paymentFile = $("#paymentFile").val();

        var method = paymentMethod == "bank_transfer" ? "Transfer Bank" : "Transfer OVO";
        var info = paymentMethod == "bank_transfer" ? "Nomor Rekening" : "Nomor OVO terdaftar";

        var showMessage = "Anda akan mengirimkan bukti pembayaran dengan metode " + method +
            " beserta dengan bukti file pembayaran dan " + info +
            " pengirim adalah : " + paymentNumber;

        if (!confirm(showMessage)) {
            return false;
        }
    });
</script>