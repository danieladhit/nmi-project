<header class="header-2 access-page-nav">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="header-top">
                    <div class="logo-area">
                        <a href="home"><img src="assets/logo/4.png" alt="" style="height: 50px; width: auto;"></a>
                    </div>
                    <div class="top-nav">
                        <a href="register" class="account-page-link">Register</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="padding-top-90 padding-bottom-90 access-page-bg">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="access-form">
                    <?php echo alert_box(); ?>
                    <div class="form-header">
                        <h5><i data-feather="user"></i>Form Login untuk Pencari Kerja</h5>
                    </div>
                    <form action="login" method="post">
                        <div class="form-group">
                            <input name="email" type="email" placeholder="Email Address" class="form-control">
                        </div>
                        <div class="form-group">
                            <input name="password" type="password" placeholder="Password" class="form-control">
                        </div>
                        <div class="more-option">
                            <!-- <div class="mt-0 terms">
                                <input class="custom-radio" type="checkbox" id="radio-4" name="termsandcondition">
                                <label for="radio-4">
                                    <span class="dot"></span> Remember Me
                                </label>
                            </div> -->
                            <!-- <a href="#">Forget Password?</a> -->
                        </div>
                        <button class="button primary-bg btn-block">Login</button>
                    </form>
                    <div class="shortcut-login">
                        <span>Atau masuk menggunakan</span>
                        <div class="buttons">
                            <a href=<?=https_url("register/fbauth/signin") ?> class="facebook"><i class="fab fa-facebook-f"></i>Facebook</a>
                            <a href="register/gauth/signin" class="google"><i class="fab fa-google"></i>Google</a>
                        </div>
                        <p>Belum mempunyai akun? <a href="register">Daftar</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>