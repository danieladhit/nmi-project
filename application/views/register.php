<header class="header-2 access-page-nav">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="header-top">
                    <div class="logo-area">
                        <a href="home"><img src="assets/logo/4.png" alt="" style="height: 50px; width: auto;"></a>
                    </div>
                    <div class="top-nav">
                        <a href="login" class="account-page-link">Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="padding-top-90 padding-bottom-90 access-page-bg">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="access-form">
                    <?php echo alert_box(); ?>
                    <div class="form-header">
                        <h5><i data-feather="edit"></i>Form Pendaftaran untuk Pencari Kerja</h5>
                    </div>
                    <!-- <div class="account-type">
                        <label for="idRegisterCan">
                            <input id="idRegisterCan" type="radio" name="register">
                            <span>Candidate</span>
                        </label>
                        <label for="idRegisterEmp">
                            <input id="idRegisterEmp" type="radio" name="register">
                            <span>Employer</span>
                        </label>
                    </div> -->
                    <form action="register" method="post">
                        <!-- <div class="form-group">
                            <input type="text" placeholder="Username" class="form-control">
                        </div> -->
                        <div class="form-group">
                            <input name="full_name" type="text" placeholder="Nama Lengkap" class="form-control">
                        </div>
                        <div class="form-group">
                            <input name="email" type="email" placeholder="Email Address" class="form-control">
                        </div>
                        <div class="form-group">
                            <input name="password" type="password" placeholder="Password" class="form-control">
                        </div>
                        <div class="form-group">
                            <input name="retype_password" type="password" placeholder="Ulangi Password" class="form-control">
                        </div>
                        <div class="form-group">
                            <select name="id_bkk_sekolah" id="bkk_sekolah" class="form-control">
                                <option value="">-- Pilih Sekolah --</option>
                                <?php foreach ($bkk as $key => $row) { ?>
                                    <option value="<?= $row->id ?>"><?= $row->full_name ?></option>
                                <?php } ?>
                                <option value="">Pelamar Umum</option>
                            </select>
                        </div>
                        <div class="more-option terms">
                            <div class="mt-0 terms">
                                <input class="custom-radio" type="checkbox" id="radio-4" name="termsandcondition" checked>
                                <label for="radio-4">
                                    <span class="dot"></span> I accept the <a href="#">terms & conditions</a>
                                </label>
                            </div>
                        </div>
                        <button class="button primary-bg btn-block">Register</button>
                    </form>
                    <div class="shortcut-login">
                        <span>Atau daftar menggunakan</span>
                        <div class="buttons">
                            <a href=<?=https_url("register/fbauth") ?> class="facebook"><i class="fab fa-facebook-f"></i>Facebook</a>
                            <a href="register/gauth" class="google"><i class="fab fa-google"></i>Google</a>
                        </div>
                        <p>Sudah mempunyai Akun? <a href="login">Masuk</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>