  <!-- BANNER -->
  <!-- <div class="bannercontainer bannerV3">
    <div class="fullscreenbanner-container">
      <div class="owl-carousel banner-slider carousel-inner">
        <?php //foreach($slider as $key => $row){ ?>
        <div class="slide">
          <a href="#"><img src="<?//=image('uploads/galeri/'.$row->galeri_slug.'/'.$row->galeri_picture, 'extra_large') ?>" class="img-responsive" alt=""></a>
        </div>
        <?php// } ?>
      </div>
    </div>
  </div>
   -->



  <div class="content_top clearfix">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <div class="content_left features">
            <h1>JUNIOR HIGH SCHOOL</h1>
            
            <ul>
              <li><i class="fa fa-check-circle-o"></i>Computer Based National Examination/ UNBK</li>
              <li><i class="fa fa-check-circle-o"></i>Cambridge Checkpoint</li>
              <li><i class="fa fa-check-circle-o"></i>Cambridge English Language Assessment</li>
              <li><i class="fa fa-check-circle-o"></i>International and National competitions: ICAS, SASMO, OSN, etc</li>
              <li><i class="fa fa-check-circle-o"></i>E-Learning Success Maker English and Math by Person</li>
              <li><i class="fa fa-check-circle-o"></i>E-Learning using Google Classroom</li>
              <li><i class="fa fa-check-circle-o"></i>Film Festival, English Camp, English Day</li>
              <li><i class="fa fa-check-circle-o"></i>Optional Program : Immersion and Science Camp</li>
            </ul>
          </div><!--end content left-->
        </div>
        <div class="col-xs-12 col-sm-6">
          <div class="content_right">
            <div class="banner" id="about_banner">
              <ul class="slides">
                <?php foreach ($slider as $key => $row): ?>

                 <li style="display: <?=$key != 0 ? 'none' : '' ?>;">
                  <img src="uploads/galeri/<?=$row->galeri_slug ?>/<?=$row->galeri_picture ?>" alt="" />
                  <div class="about_caption">
                    <h2></h2>
                  </div><!--end banner_caption-->
                </li>

                <?php endforeach ?>
                <!-- <li style="display:none;">
                  <img src="img/about/about-slide-02.jpg" alt="" />
                  <div class="about_caption">
                    <h2>Nice classrooms with wifi</h2>
                  </div>end banner_caption
                </li>
                
                <li style="display:none;">
                  <img src="img/about/about-slide-03.jpg" alt="" />
                  <div class="about_caption">
                    <h2>Rich library - open 24/7</h2>
                  </div>end banner_caption
                </li> -->
              </ul>
            </div><!--end banner-->
          </div><!--end content left-->
        </div>
      </div><!--end row-->
    </div><!--end container-->
    </div><!--end content top-->


     <div class="clearfix padding gallery-section">
    <div class="container">
      <div class="sectionTitle title-block">
        <h3>Facilities</h3>
      </div>
      <a class="more" href="<?=base_url('images') ?>">View All &gt;</a>
      <div class="custom photo_gallery">
        <ul class="gallery">
          <?php foreach($galery_street_view as $key => $row){ ?>
          <li>
            <a class="popup-gmaps" href="<?=get_iframe_src($row->galeri_description) ?>" title="<?=$row->galeri_title ?>">
              <!-- <img src="<?//=streetview_image_url($row->galeri_link) ?>" alt=""> -->
              <img src="uploads/galeri/<?=$row->galeri_slug ?>/<?=$row->galeri_picture ?>" alt="">
              <div class="overlay">
                <span class="zoom">
                  <i class="fa fa-search"></i>
                </span>
              </div>
            </a>
          </li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>