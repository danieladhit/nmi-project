<!-- Breadcrumb -->
<div class="alice-bg padding-top-70 padding-bottom-70">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="breadcrumb-area">
                    <h1>Dashboard</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6">
                <div class="breadcrumb-form">
                    <form class="work-on-progress" action="">
                        <input type="text" placeholder="Enter Keywords">
                        <button><i data-feather="search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->

<div class="alice-bg section-padding-bottom">
    <div class="container no-gliters">
        <div class="row no-gliters">
            <div class="col">
                <div class="dashboard-container">
                    <div class="dashboard-content-wrapper">
                        <div class="dashboard-section user-statistic-block">
                            <div class="user-statistic">
                                <i data-feather="pie-chart"></i>
                                <h3>132</h3>
                                <span>Di Lihat Perusahaan</span>
                            </div>
                            <div class="user-statistic">
                                <i data-feather="briefcase"></i>
                                <h3>12</h3>
                                <span>Pekerjaan di Apply</span>
                            </div>
                            <div class="user-statistic">
                                <i data-feather="heart"></i>
                                <h3>32</h3>
                                <span>Pekerjaan di Tandai</span>
                            </div>
                        </div>
                        <div class="dashboard-section dashboard-view-chart">
                            <canvas id="view-chart" width="400" height="200"></canvas>
                        </div>
                        <div class="dashboard-section dashboard-recent-activity">
                            <h4 class="title">Recent Activity</h4>
                            <div class="activity-list">
                                <i class="fas fa-bolt"></i>
                                <div class="content">
                                    <h5>Selamat Datang, <?= $pencaker->full_name ?></h5>
                                    <span class="time">Register at <?= date('d M Y H:i:s', strtotime($pencaker->created_at)) ?></span>
                                </div>
                                <!-- <div class="close-activity">
                                    <i class="fas fa-times"></i>
                                </div> -->
                            </div>
                            <div class="activity-list">
                                <i class="fas fa-arrow-circle-down"></i>
                                <div class="content">
                                    <h5>Anda sudah melakukan pembayaran untuk menjadi member.</h5>
                                    <span class="time">beberapa jam yang lalu</span>
                                </div>
                                <!-- <div class="close-activity">
                                    <i class="fas fa-times"></i>
                                </div> -->
                            </div>
                            <div class="activity-list">
                                <i class="fas fa-check-square"></i>
                                <div class="content">
                                    <h5>Pembayaran Anda sudah di Verifikasi</h5>
                                    <span class="time">beberapa jam yang lalu</span>
                                </div>
                                <!-- <div class="close-activity">
                                    <i class="fas fa-times"></i>
                                </div> -->
                            </div>
                            <div class="activity-list">
                                <i class="fas fa-check-square"></i>
                                <div class="content">
                                    <h5>Hai <?= $pencaker->full_name ?>, Anda telah menjadi member.</h5>
                                    <span class="time">beberapa jam yang lalu</span>
                                </div>
                                <!-- <div class="close-activity">
                                    <i class="fas fa-times"></i>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <!-- include dashboard user  -->
                    <?php $this->load->view('pencari_kerja/dashboard_sidebar') ?>
                 
                </div>
            </div>
        </div>
    </div>
</div>