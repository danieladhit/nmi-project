<div class="custom_content clearfix">
	<div class="container">
		<?php $this->load->view('_partials/breadcrumb'); ?>
		<h3>Videos</h3>
		<br>
		<div class="photo_gallery custom">
			<ul class="gallery popup-gallery gallery-3col">
				<?php foreach($videos as $key => $row){ ?>
				<li>
					<a class="popup-youtube" href="<?=$row->video_link ?>">
						<img src="http://img.youtube.com/vi/<?=youtube_id($row->video_link) ?>/hqdefault.jpg" alt="">
						<div class="overlay">
							<span class="zoom">
								<i class="fa fa-search"></i>
							</span>
						</div>
					</a>
				</li>
				<?php } ?>
			</ul>
		</div>

	</div>
</div>