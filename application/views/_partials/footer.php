<!-- NewsLetter -->
<div class="newsletter-bg padding-top-90 padding-bottom-90">
	<div class="container">
		<div class="row">
			<div class="col-xl-5 col-lg-6">
				<div class="newsletter-wrap">
					<h3>Informasi Lowongan Kerja</h3>
					<p>Dapatkan e-mail mengenai lowongan kerja terbaru.</p>
					<form id="formNewsletter" action="" class="newsletter-form form-inline">
						<div class="form-group">
							<input id="newsletterEmail" type="text" class="form-control" placeholder="Alamat email...">
						</div>
						<button type="submit" class="btn button">Kirim<i class="fas fa-caret-right"></i></button>
						<p class="newsletter-error">0 - Please enter a value</p>
						<p class="newsletter-success">Thank you for subscribing!</p>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- NewsLetter End -->

<!-- Footer -->
<footer class="footer-bg">
	<div class="footer-top border-bottom padding-bottom-40" style="padding-top: 40px;">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="footer-logo">
						<a href="#">
							<img src="assets/logo/1.png" style="height: 150px" class="img-fluid" alt="">
						</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="footer-social">
						<ul class="social-icons">
							<li><a class="work-on-progress" href="javascript:void(0)"><i data-feather="facebook"></i></a></li>
							<li><a class="work-on-progress" href="javascript:void(0)"><i data-feather="twitter"></i></a></li>
							<li><a class="work-on-progress" href="javascript:void(0)"><i data-feather="linkedin"></i></a></li>
							<li><a class="work-on-progress" href="javascript:void(0)"><i data-feather="instagram"></i></a></li>
							<li><a class="work-on-progress" href="javascript:void(0)"><i data-feather="youtube"></i></a></li>
						</ul>
						<!-- <select class="selectpicker select-language" id="select-country">
							<option value="United States of America" selected>EN</option>
							<option value="United Kingdom">GB</option>
							<option value="Spain">ES</option>
							<option value="Portugal">PT</option>
						</select> -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-widget-wrapper padding-bottom-60 padding-top-80">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-sm-6">
					<div class="footer-widget widget-about">
						<h4>Tentang Kami</h4>
						<div class="widget-inner">
							<p class="description">PT. NANO MEGA INDUSTRIES merupakan anak perusahaan dari Nano Group, yang bergerak di bidang Pelayanan Jasa SDM terintegrasi pertama di Indonesia yang menerapkan program e-recruitment dengan memanfaatkan One Integrated Services Technology. Program e-recruitment ini adalah sebuah terobosan baru dalam dunia jasa Pengelolaan, Konsultasi dan Pengadaan jasa tenaga kerja di Perusahaan yang dimana mengintegrasikan seluruh komponen sumber daya yang terlibat secara langsung dalam kegiatan ini.</p>
							<span class="about-contact"><i data-feather="phone-forwarded"></i>08886111509</span>
							<span class="about-contact"><i data-feather="mail"></i>info@namendustri.com</span>
						</div>
					</div>
				</div>
				<div class="col-lg-2 offset-lg-1 col-sm-6">
					<div class="footer-widget footer-shortcut-link">
						<h4>Informasi</h4>
						<div class="widget-inner">
							<ul>
								<li><a class="work-on-progress" href="javascript:void(0)">Tentang Kami</a></li>
								<li><a class="work-on-progress" href="javascript:void(0)">Kontak Kami</a></li>
								<!-- <li><a href="#">Privacy Policy</a></li>
								<li><a href="#">Terms &amp; Conditions</a></li> -->
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-sm-6">
					<div class="footer-widget footer-shortcut-link">
						<h4>Pencari Kerja</h4>
						<div class="widget-inner">
							<ul>
								<li><a class="work-on-progress" href="javascript:void(0)">Buat Akun</a></li>
								<li><a class="work-on-progress" href="javascript:void(0)">Lowongan Pekerjaan</a></li>
								<li><a class="work-on-progress" href="javascript:void(0)">FAQ</a></li>
								<!-- <li><a href="#">Video Guides</a></li> -->
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-sm-6">
					<div class="footer-widget footer-shortcut-link">
						<h4>Perusahaan</h4>
						<div class="widget-inner">
							<ul>
								<li><a class="work-on-progress" href="javascript:void(0)">Buat Akun</a></li>
								<li><a class="work-on-progress" href="javascript:void(0)">Buat Lowongan Pekerjaan</a></li>
								<li><a class="work-on-progress" href="javascript:void(0)">FAQ</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom-area">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="footer-bottom border-top">
						<div class="row">
							<div class="col-xl-4 col-lg-5 order-lg-2">
								<div class="footer-app-download">
									<a href="javascript:void(0)" class="apple-app work-on-progress">Apple Store</a>
									<a href="javascript:void(0)" class="android-app work-on-progress">Google Play</a>
								</div>
							</div>
							<div class="col-xl-4 col-lg-4 order-lg-1">
								<p class="copyright-text">Copyright <a href="home">Nano Mega Industri</a> 2020, All right reserved</p>
							</div>
							<div class="col-xl-4 col-lg-3 order-lg-3">
								<div class="back-to-top">
									<a href="#">Back to top<i class="fas fa-angle-up"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- Footer End -->

<script>
	function isValidEmail(emailAddress) {
		var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

		return pattern.test(emailAddress);

	}

	$("#formNewsletter").submit(function(e) {
		var email = $("#newsletterEmail").val();

		if (email == '') {
			$('.newsletter-error').html('Email tidak boleh kosong').fadeIn().delay(3000).fadeOut();
		} else if (!isValidEmail(email)) {
			$('.newsletter-error').html('Email tidak valid').fadeIn().delay(3000).fadeOut();
		} else {
			$('.newsletter-success').html('Email Anda sudah terdaftar, Terima kasih').fadeIn().delay(3000).fadeOut();
		}

	//	if (resp.result === 'success') {
    //     $('.newsletter-success').html(resp.msg).fadeIn().delay(3000).fadeOut();
    //   } else if (resp.result === 'error') {
    //     $('.newsletter-error').html(resp.msg).fadeIn().delay(3000).fadeOut();
    //   }

		return false;
	});
</script>