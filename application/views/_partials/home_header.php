<!-- Header -->
<header>
    <nav class="navbar navbar-expand-xl absolute-nav transparent-nav cp-nav navbar-light bg-light fluid-nav">
        <a class="navbar-brand" href="index.html">
            <img src="assets/logo/white/1.png" style="height: 60px; width: auto;" class="img-fluid" alt="">
        </a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- <ul class="navbar-nav mr-auto job-browse">
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Browse Jobs</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li class="search-by">
                            <h5>BY Category</h5>
                            <ul>
                                <li><a href="#">Accounting / Finance <span>(233)</span></a></li>
                                <li><a href="#">Education <span>(46)</span></a></li>
                                <li><a href="#">Design & Creative <span>(156)</span></a></li>
                                <li><a href="#">Health Care <span>(98)</span></a></li>
                                <li><a href="#">Engineer & Architects <span>(188)</span></a></li>
                                <li><a href="#">Marketing & Sales <span>(124)</span></a></li>
                                <li><a href="#">Garments / Textile <span>(584)</span></a></li>
                                <li><a href="#">Customer Support <span>(233)</span></a></li>
                            </ul>
                        </li>
                        <li class="search-by">
                            <h5>BY LOcation</h5>
                            <ul>
                                <li><a href="#">New York City <span>(508)</span></a></li>
                                <li><a href="#">Washington, D.C <span>(96)</span></a></li>
                                <li><a href="#">Chicago <span>(155)</span></a></li>
                                <li><a href="#">San Francisco <span>(24)</span></a></li>
                                <li><a href="#">Los Angeles <span>(268)</span></a></li>
                                <li><a href="#">Boston <span>(46)</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul> -->
            <ul class="navbar-nav ml-auto main-nav">
                <!-- <li class="menu-item active"><a title="Home" href="home-1.html">Home</a></li> -->
                <!-- <li class="menu-item dropdown">
                    <a title="" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">Lowongan Pekerjaan</a>
                    <ul class="dropdown-menu">
                        <li class="menu-item"><a href="job-listing.html">Job Listing</a></li>
                        <li class="menu-item"><a href="job-listing-with-map.html">Job Listing With Map</a></li>
                        <li class="menu-item"><a href="job-details.html">Job Details</a></li>
                        <li class="menu-item"><a href="post-job.html">Post Job</a></li>
                    </ul>
                </li> -->
                <li class="menu-item dropdown">
                    <a title="" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">Perusahaan</a>
                    <ul class="dropdown-menu">
                        <li class="menu-item work-on-progress"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="menu-item work-on-progress"><a href="javascript:void(0)">Ubah Profil</a></li>
                        <li class="menu-item work-on-progress"><a href="javascript:void(0)">Managemen Pelamar</a></li>
                        <li class="menu-item work-on-progress"><a href="javascript:void(0)">Managemen Lowongan Pekerjaan</a></li>
                        <li class="menu-item work-on-progress"><a href="javascript:void(0)">Message</a></li>
                        <li class="menu-item work-on-progress"><a href="javascript:void(0)">Post Lowongan Pekerjaan</a></li>
                    </ul>
                </li>
                <li class="menu-item dropdown">
                    <a title="" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">Pencari Kerja</a>
                    <ul class="dropdown-menu">
                        <li class="menu-item"><a href="pencari_kerja">Dashboard</a></li>
                        <li class="menu-item"><a href="pencari_kerja">Ubah Profil</a></li>
                        <li class="menu-item"><a href="pencari_kerja">Lamaran</a></li>
                        <li class="menu-item"><a href="pencari_kerja">Lowongan Tersimpan</a></li>
                        <li class="menu-item"><a href="pencari_kerja">Message</a></li>
                    </ul>
                </li>

                <!-- <li class="menu-item post-job"><a title="Title" href="post-job.html"><i class="fas fa-plus"></i>Buat Lowongan Pekerjaan</a></li> -->
            </ul>
            <ul class="navbar-nav ml-auto account-nav">
                <?php $user = get_user(); ?>
                <?php if (empty($user)) { ?>
                    <li class="menu-item login-popup"><button title="Title" type="button" data-toggle="modal" data-target="#exampleModalLong">Login</button></li>
                    <li class="menu-item login-popup"><button title="Title" type="button" data-toggle="modal" data-target="#exampleModalLong2">Registrasi</button></li>
                <?php } else { ?>
                    <li class="dropdown menu-item header-top-notification">
                        <a href="#" class="notification-button"></a>
                        <div class="notification-card">
                            <div class="notification-head">
                                <span>Notifikasi</span>
                                <!-- <a href="#">Mark all as read</a> -->
                            </div>
                            <div class="notification-body">
                                <a href="#" class="notification-list">
                                    <i class="fas fa-user"></i>
                                    <p>Selamat Datang, <?= $user['full_name'] ?></p>
                                    <span class="time">Register at <?= date('d M Y H:i:s', strtotime($user['created_at'])) ?></span>
                                </a>

                                <?php if ($user['payment_status'] == 0) { ?>
                                    <a href="pencari_kerja" class="notification-list">
                                        <i class="fas fa-users"></i>
                                        <p>Silahkan lakukan pembayaran untuk menjadi Member</p>
                                        <span class="time">1 menit yang lalu</span>
                                    </a>
                                <?php } else if ($user['payment_status'] == 1) { ?>
                                    <a href="#" class="notification-list">
                                        <i class="fas fa-users"></i>
                                        <p>Anda sudah melakukan pembayaran untuk menjadi member</p>
                                        <span class="time">1 menit yang lalu</span>
                                    </a>
                                    <a href="pencari_kerja" class="notification-list">
                                        <i class="fas fa-arrow-circle-down"></i>
                                        <p>Akun Anda sedang di verifikasi oleh sistem kami</p>
                                        <span class="time">1 menit yang lalu</span>
                                    </a>
                                <?php } else { ?>
                                    <a href="#" class="notification-list">
                                        <i class="fas fa-users"></i>
                                        <p>Anda sudah melakukan pembayaran untuk menjadi member</p>
                                        <span class="time">1 menit yang lalu</span>
                                    </a>
                                    <a href="#" class="notification-list">
                                        <i class="fas fa-arrow-circle-down"></i>
                                        <p>Pembayaran sudah di verifikasi</p>
                                        <span class="time">1 menit yang lalu</span>
                                    </a>
                                    <a href="#" class="notification-list">
                                        <i class="fas fa-users"></i>
                                        <p>Hai <?= $user['full_name'] ?>, Anda telah menjadi member</p>
                                        <span class="time">1 menit yang lalu</span>
                                    </a>
                                <?php } ?>

                            </div>
                            <div class="notification-footer">
                                <a href="#">See all notification</a>
                            </div>
                        </div>
                    </li>
                    <li class="menu-item login-popup"><a href="login/destroy" title="Title" type="button">Logout</a></li>
                <?php } ?>
            </ul>
        </div>
    </nav>
    <!-- Modal -->
    <div class="account-entry">
        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><i data-feather="user"></i>Login</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="login" method="post">
                            <div class="form-group">
                                <input name="email" type="email" placeholder="Email Address" class="form-control">
                            </div>
                            <div class="form-group">
                                <input name="password" type="password" placeholder="Password" class="form-control">
                            </div>
                            <div class="more-option">
                                <!-- <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                    <label class="form-check-label" for="defaultCheck1">
                                        Remember Me
                                    </label>
                                </div>
                                <a href="#">Forget Password?</a> -->
                            </div>
                            <button type="submit" class="button primary-bg btn-block">Login</button>
                        </form>
                        <div class="shortcut-login">
                            <span>Or connect with</span>
                            <div class="buttons">
                                <a href=<?=https_url("register/fbauth/signin") ?> class="facebook"><i class="fab fa-facebook-f"></i>Facebook</a>
                                <a href="register/gauth/signin" class="google"><i class="fab fa-google"></i>Google</a>
                            </div>
                            <p>Don't have an account? <a href="register">Register</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModalLong2" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><i data-feather="edit"></i>Registrasi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!-- <div class="account-type">
                            <label for="idRegisterCan">
                                <input id="idRegisterCan" type="radio" name="register">
                                <span>Candidate</span>
                            </label>
                            <label for="idRegisterEmp">
                                <input id="idRegisterEmp" type="radio" name="register">
                                <span>Employer</span>
                            </label>
                        </div> -->
                        <form action="register" method="post">
                            <div class="form-group">
                                <input name="full_name" type="text" placeholder="Nama Lengkap" class="form-control">
                            </div>
                            <div class="form-group">
                                <input name="email" type="email" placeholder="Email Address" class="form-control">
                            </div>
                            <div class="form-group">
                                <input name="password" type="password" placeholder="Password" class="form-control">
                            </div>
                            <div class="form-group">
                                <input name="retype_password" type="password" placeholder="Ulangi Password" class="form-control">
                            </div>
                            <div class="form-group">
                                <select name="id_bkk_sekolah" id="bkk_sekolah" class="form-control">
                                    <option value="">-- Pilih Sekolah --</option>
                                    <?php foreach ($bkk as $key => $row) { ?>
                                        <option value="<?= $row->id ?>"><?= $row->full_name ?></option>
                                    <?php } ?>
                                    <option value="">Pelamar Umum</option>
                                </select>
                            </div>
                            <div class="more-option terms">
                                <div class="form-check">
                                    <input class="form-check-input" name="termsandcondition" type="checkbox" id="defaultCheck2" checked>
                                    <label class="form-check-label" for="defaultCheck2">
                                        I accept the <a href="#">terms & conditions</a>
                                    </label>
                                </div>
                            </div>
                            <button type="submit" class="button primary-bg btn-block">Register</button>
                        </form>
                        <div class="shortcut-login">
                            <span>Or connect with</span>
                            <div class="buttons">
                                <a href=<?=https_url("register/fbauth") ?> class="facebook"><i class="fab fa-facebook-f"></i>Facebook</a>
                                <a href="register/gauth" class="google"><i class="fab fa-google"></i>Google</a>
                            </div>
                            <p>Already have an account? <a href="login">Login</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Header End -->


<!-- Banner -->
<div class="banner banner-1 banner-1-bg">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="banner-content">
                    <h1>100+ Lowongan Pekerjaan</h1>
                    <p>Daftar segera untuk mencari ratusan lowongan pekerjaan, mencari pekerja handal & Peluang karir terbuka lebar!</p>
                    <a href="register" class="button">Daftar Sekarang</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Banner End -->

<!-- Search and Filter -->
<div class="searchAndFilter-wrapper">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="searchAndFilter-block">
                    <div class="searchAndFilter">
                        <!-- <form action="#" class="search-form">
                            <input type="text" placeholder="Enter Keywords">
                            <select class="selectpicker" id="search-location">
                                <option value="" selected>Location</option>
                                <option value="california">California</option>
                                <option value="las-vegas">Las Vegas</option>
                                <option value="new-work">New Work</option>
                                <option value="carolina">Carolina</option>
                                <option value="chicago">Chicago</option>
                                <option value="silicon-vally">Silicon Vally</option>
                                <option value="washington">Washington DC</option>
                                <option value="neveda">Neveda</option>
                            </select>
                            <select class="selectpicker" id="search-category">
                                <option value="" selected>Category</option>
                                <option value="real-state">Real State</option>
                                <option value="vehicales">Vehicales</option>
                                <option value="electronics">Electronics</option>
                                <option value="beauty">Beauty</option>
                                <option value="furnitures">Furnitures</option>
                            </select>
                            <button class="button primary-bg"><i class="fas fa-search"></i>Search Job</button>
                        </form> -->
                        <div class="filter-categories">
                            <h4>Job Category</h4>
                            <ul>
                                <li><a href="javasript:void(0)" class="work-on-progress"><i data-feather="bar-chart-2"></i>Accounting / Finance <span>(33)</span></a></li>
                                <li><a href="javasript:void(0)" class="work-on-progress"><i data-feather="edit-3"></i>Human Resource Provider <span>(46)</span></a></li>
                                <li><a href="javasript:void(0)" class="work-on-progress"><i data-feather="feather"></i>Design & Creative <span>(56)</span></a></li>
                                <li><a href="javasript:void(0)" class="work-on-progress"><i data-feather="briefcase"></i>Manufacturing / industries <span>(28)</span></a></li>
                                <li><a href="javasript:void(0)" class="work-on-progress"><i data-feather="package"></i>Engineer & Architects <span>(88)</span></a></li>
                                <li><a href="javasript:void(0)" class="work-on-progress"><i data-feather="pie-chart"></i>Marketing & Sales <span>(24)</span></a></li>
                                <li><a href="javasript:void(0)" class="work-on-progress"><i data-feather="command"></i>Garments / Textile <span>(84)</span></a></li>
                                <li><a href="javasript:void(0)" class="work-on-progress"><i data-feather="globe"></i>Customer Support <span>(33)</span></a></li>
                                <li><a href="javasript:void(0)" class="work-on-progress"><i data-feather="shield"></i>Security Services <span>(15)</span></a></li>
                                <li><a href="javasript:void(0)" class="work-on-progress"><i data-feather="radio"></i>Cleaning Service / House Keeping <span>(93)</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Search and Filter End -->