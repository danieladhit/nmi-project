<div class="gratyo-home">
    <section id="slider">
        <div class="container-fluid no-padding">
            <div class="swiper-container">
                <div class="swiper-wrapper">

                    <div class="swiper-slide">
                        <a href="<?= base_url() ?>themes/gratyo/http://gratyo.com/event/jakarta-digital-marketing-transformation1">
                            <img src="<?= base_url() ?>themes/gratyo/img/content/images/CI1904090112019-04-09_09_39_51photo_thumb.jpg" alt="" class="image hidden-xs">
                        </a>
                        <a href="<?= base_url() ?>themes/gratyo/http://gratyo.com/event/jakarta-digital-marketing-transformation1">
                            <img src="<?= base_url() ?>themes/gratyo/img/content/images/CI1904090122019-04-09_09_40_20photo_thumb.jpg" alt="" class="image-mobile hidden-lg">
                        </a>
                    </div>

                    <div class="swiper-slide">
                        <a href="<?= base_url() ?>themes/gratyo/http://gratyo.com/event/jakarta-digital-marketing-transformation1">
                            <img src="<?= base_url() ?>themes/gratyo/img/content/images/CI1904090032019-04-09_09_23_56photo_thumb.jpg" alt="" class="image hidden-xs">
                        </a>
                        <a href="<?= base_url() ?>themes/gratyo/http://gratyo.com/event/jakarta-digital-marketing-transformation1">
                            <img src="<?= base_url() ?>themes/gratyo/img/content/images/CI1904110072019-04-11_15_18_34photo_thumb.jpg" alt="" class="image-mobile hidden-lg">
                        </a>
                    </div>

                    <div class="swiper-slide">
                        <a href="<?= base_url() ?>themes/gratyo/http://gratyo.com/event/jakarta-digital-marketing-transformation1">
                            <img src="<?= base_url() ?>themes/gratyo/img/content/images/CI1904090012019-04-09_09_15_04photo_thumb.jpg" alt="" class="image hidden-xs">
                        </a>
                        <a href="<?= base_url() ?>themes/gratyo/http://gratyo.com/event/jakarta-digital-marketing-transformation1">
                            <img src="<?= base_url() ?>themes/gratyo/img/content/images/CI1904090022019-04-09_09_20_30photo_thumb.jpg" alt="" class="image-mobile hidden-lg">
                        </a>
                    </div>

                    <div class="swiper-slide">
                        <a href="<?= base_url() ?>themes/gratyo/http://gratyo.com/event/jakarta-digital-marketing-transformation1">
                            <img src="<?= base_url() ?>themes/gratyo/img/content/images/CI1904090072019-04-09_09_34_45photo_thumb.jpg" alt="" class="image hidden-xs">
                        </a>
                        <a href="<?= base_url() ?>themes/gratyo/http://gratyo.com/event/jakarta-digital-marketing-transformation1">
                            <img src="<?= base_url() ?>themes/gratyo/img/content/images/CI1904110082019-04-11_15_19_06photo_thumb.jpg" alt="" class="image-mobile hidden-lg">
                        </a>
                    </div>

                    <div class="swiper-slide">
                        <a href="<?= base_url() ?>themes/gratyo/http://gratyo.com/event/jakarta-digital-marketing-transformation1">
                            <img src="<?= base_url() ?>themes/gratyo/img/content/images/CI1904090052019-04-09_09_31_15photo_thumb.jpg" alt="" class="image hidden-xs">
                        </a>
                        <a href="<?= base_url() ?>themes/gratyo/http://gratyo.com/event/jakarta-digital-marketing-transformation1">
                            <img src="<?= base_url() ?>themes/gratyo/img/content/images/CI1904090062019-04-09_09_31_49photo_thumb.jpg" alt="" class="image-mobile hidden-lg">
                        </a>
                    </div>

                    <div class="swiper-slide">
                        <a href="<?= base_url() ?>themes/gratyo/http://gratyo.com/event/jakarta-digital-marketing-transformation1">
                            <img src="<?= base_url() ?>themes/gratyo/img/content/images/CI1904090092019-04-09_09_37_12photo_thumb.jpg" alt="" class="image hidden-xs">
                        </a>
                        <a href="<?= base_url() ?>themes/gratyo/http://gratyo.com/event/jakarta-digital-marketing-transformation1">
                            <img src="<?= base_url() ?>themes/gratyo/img/content/images/CI1904090102019-04-09_09_37_39photo_thumb.jpg" alt="" class="image-mobile hidden-lg">
                        </a>
                    </div>

                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"><i class="fa fa-angle-right"></i></div>
                <div class="swiper-button-prev"><i class="fa fa-angle-left"></i></div>
            </div>
        </div>
    </section>
</div>