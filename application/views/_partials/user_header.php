<?php $user = (array) $pencaker; ?>
<header class="header-2">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="header-top">
                    <div class="logo-area">
                        <a href="index.html"><img src="assets/logo/4.png" alt=""></a>
                    </div>
                    <div class="header-top-toggler">
                        <div class="header-top-toggler-button"></div>
                    </div>
                    <div class="top-nav">
                        <div class="dropdown header-top-notification">
                            <a href="#" class="notification-button">Notifikasi</a>
                            <div class="notification-card">
                                <div class="notification-head">
                                    <span>Notifikasi</span>
                                    <!-- <a href="#">Mark all as read</a> -->
                                </div>
                                <div class="notification-body">
                                    <a href="#" class="notification-list">
                                        <i class="fas fa-user"></i>
                                        <p>Selamat Datang, <?= $user['full_name'] ?></p>
                                        <span class="time">Register at <?= date('d M Y H:i:s') ?></span>
                                    </a>

                                    <?php if ($user['payment_status'] == 0) { ?>
                                        <a href="pencari_kerja" class="notification-list">
                                            <i class="fas fa-users"></i>
                                            <p>Silahkan lakukan pembayaran untuk menjadi Member</p>
                                            <span class="time">1 menit yang lalu</span>
                                        </a>
                                    <?php } else if ($user['payment_status'] == 1) { ?>
                                        <a href="#" class="notification-list">
                                            <i class="fas fa-users"></i>
                                            <p>Anda sudah melakukan pembayaran untuk menjadi member</p>
                                            <span class="time">1 menit yang lalu</span>
                                        </a>
                                        <a href="pencari_kerja" class="notification-list">
                                            <i class="fas fa-arrow-circle-down"></i>
                                            <p>Akun Anda sedang di verifikasi oleh sistem kami</p>
                                            <span class="time">1 menit yang lalu</span>
                                        </a>
                                    <?php } else { ?>
                                        <a href="#" class="notification-list">
                                            <i class="fas fa-users"></i>
                                            <p>Anda sudah melakukan pembayaran untuk menjadi member</p>
                                            <span class="time">1 menit yang lalu</span>
                                        </a>
                                        <a href="#" class="notification-list">
                                            <i class="fas fa-arrow-circle-down"></i>
                                            <p>Pembayaran sudah di verifikasi</p>
                                            <span class="time">1 menit yang lalu</span>
                                        </a>
                                        <a href="#" class="notification-list">
                                            <i class="fas fa-users"></i>
                                            <p>Hai <?= $user['full_name'] ?>, Anda telah menjadi member</p>
                                            <span class="time">1 menit yang lalu</span>
                                        </a>
                                    <?php } ?>
                                </div>
                                <div class="notification-footer">
                                    <a href="#">See all notification</a>
                                </div>
                            </div>
                        </div>
                        <div class="dropdown header-top-account">
                            <a href="#" class="account-button">Akun Saya</a>
                            <div class="account-card">
                                <div class="header-top-account-info">
                                    <a href="#" class="account-thumb">
                                        <img src="<?= $user['photo_profile'] ?>" class="img-fluid" alt="">
                                    </a>
                                    <div class="account-body">
                                        <h5><a href="#"><?= $user['full_name'] ?></a></h5>
                                        <?php
                                        if (!empty($user['email'])) {
                                            $email = $user['email'];
                                        } else if (!empty($user['google_email'])) {
                                            $email = $user['google_email'];
                                        } else {
                                            $email = $user['facebook_email'];
                                        }
                                        ?>
                                        <span class="mail"><?= $email ?></span>
                                    </div>
                                </div>
                                <ul class="account-item-list">
                                    <!-- <li><a href="#"><span class="ti-user"></span>Account</a></li>
                                    <li><a href="#"><span class="ti-settings"></span>Settings</a></li> -->
                                    <li><a href="login/destroy"><span class="ti-power-off"></span>Log Out</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- <select class="selectpicker select-language" data-width="fit">
                            <option data-content='<span class="flag-icon flag-icon-us"></span> English'>English</option>
                            <option data-content='<span class="flag-icon flag-icon-mx"></span> Español'>Español</option>
                        </select> -->
                    </div>
                </div>
                <nav class="navbar navbar-expand-lg cp-nav-2">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                            <li class="menu-item active"><a title="Home" href="home">Home</a></li>
                            <li class="menu-item"><a href="#">Lowongan Kerja</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>