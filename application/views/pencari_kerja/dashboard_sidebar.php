<div class="dashboard-sidebar">
    <div class="user-info">
        <div class="thumb">
            <img src="<?= $pencaker->photo_profile ?>" class="img-fluid" alt="">
        </div>
        <div class="user-body">
            <h5><?= $pencaker->full_name ?></h5>
            <span><?= $pencaker->email ?></span>
        </div>
    </div>
    <div class="profile-progress">
        <div class="progress-item">
            <div class="progress-head">
                <p class="progress-on">Profile</p>
            </div>
            <div class="progress-body">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0;"></div>
                </div>
                <p class="progress-to">40%</p>
            </div>
        </div>
    </div>
    <div class="dashboard-menu">
        <ul>
            <li class="<?=$dashboard_sidebar == 'dashboard' ? 'active' : '' ?>"><i class="fas fa-home"></i><a href="pencari_kerja">Dashboard</a></li>
            <li class="<?=$dashboard_sidebar == 'edit_profile' ? 'active' : '' ?>"><i class="fas fa-user"></i><a href="pencari_kerja/edit_profile">Ubah Profile</a></li>
            <li class="<?=$dashboard_sidebar == 'resume' ? 'active' : '' ?>"><i class="fas fa-file-alt"></i><a href="pencari_kerja/resume">Lamaran</a></li>
            <li class="<?=$dashboard_sidebar == 'edit_resume' ? 'active' : '' ?>"><i class="fas fa-edit"></i><a href="pencari_kerja/edit_resume">Ubah Lamaran</a></li>
            <li class="<?=$dashboard_sidebar == 'bookmark' ? 'active' : '' ?>"><i class="fas fa-heart"></i><a class="work-on-progress" href="javascript:void(0)">Bookmark</a></li>
            <li class="<?=$dashboard_sidebar == 'job_apply' ? 'active' : '' ?>"><i class="fas fa-check-square"></i><a class="work-on-progress" href="javascript:void(0)">Pekerjaan di Apply</a></li>
        </ul>
        <ul class="delete">
            <li><i class="fas fa-power-off"></i><a href="login/destroy">Logout</a></li>
        </ul>
    </div>
</div>