<!-- Breadcrumb -->
<div class="alice-bg padding-top-70 padding-bottom-70">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="breadcrumb-area">
                    <h1>Ubah Profil</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Ubah Profil</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6">
                <div class="breadcrumb-form">
                    <form class="work-on-progress" action="">
                        <input type="text" placeholder="Enter Keywords">
                        <button><i data-feather="search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->

<div class="alice-bg section-padding-bottom">
    <div class="container no-gliters">
        <div class="row no-gliters">
            <div class="col">
                <div class="dashboard-container">
                    <div class="dashboard-content-wrapper">
                        <form action="pencari_kerja/edit_profile" method="post" class="dashboard-form" enctype="multipart/form-data">
                            <div class="dashboard-section upload-profile-photo">
                                <div class="update-photo">
                                    <img class="image" src="<?= $pencaker->photo_profile ?>" alt="">
                                </div>
                                <div class="file-upload">
                                    <input name="photo_profile" type="file" class="file-input">Ganti Foto
                                </div>
                            </div>
                            <div class="dashboard-section basic-info-input">
                                <h4><i data-feather="user-check"></i>Basic Info</h4>
                                <?=alert_box() ?>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nama Lengkap</label>
                                    <div class="col-sm-9">
                                        <input required type="text" name="full_name" value="<?=$pencaker->full_name ?>" class="form-control" placeholder="Full Name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Email Address</label>
                                    <div class="col-sm-9">
                                        <input readonly type="text" name="email" value="<?=$pencaker->email ?>" class="form-control" placeholder="email@example.com">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Phone</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="phone_number" value="<?=$pencaker->phone_number ?>" class="form-control" placeholder="contoh : 081244446666">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Address</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="address" value="<?=$pencaker->address ?>" class="form-control" placeholder="Tempat tinggal saat ini..">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Keahlian</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="expertise" value="<?=$pencaker->expertise ?>" class="form-control" placeholder="Keahlian utama Anda..">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tentang Saya</label>
                                    <div class="col-sm-9">
                                        <textarea name="about_me" class="form-control" placeholder="Perkenalkan tentang diri Anda.."><?=$pencaker->about_me ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="dashboard-section basic-info-input">
                                <?php if (empty($pencaker->google_id) && empty($pencaker->facebook_id)) { ?>
                                    <h4><i data-feather="lock"></i>Change Password</h4>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Current Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" name="current_password" class="form-control" placeholder="Current Password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">New Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" name="new_password" class="form-control" placeholder="New Password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Retype Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" name="retype_password" class="form-control" placeholder="Retype Password">
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="button">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <!-- include dashboard user  -->
                    <?php $this->load->view('pencari_kerja/dashboard_sidebar') ?>

                </div>
            </div>
        </div>
    </div>
</div>