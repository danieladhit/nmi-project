<div id="content-wrapper">

        <div class="page-header">
            <h1><?php echo strtoupper($heading)?></h1>
        </div> <!-- / .page-header -->

        <div class="row">
            <div class="col-sm-12">

                <!-- Javascript -->
                <script>
                    init.push(function () {
                        $('#tooltips-demo button').tooltip();
                        $('#tooltips-demo a').tooltip();
                        $('#jq-datatables-example0').dataTable();
                        $('#jq-datatables-example0_wrapper .table-caption').text('Daftar Pendaftar Materi Gratis');
                        $('#jq-datatables-example0_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
                    });
                </script>
                <!-- / Javascript -->

                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title"><?php echo ucwords($heading)?></span>
                    </div>
                    <div class="panel-body">
                        <?php $this->load->view('admin/field/flash_message'); ?>

                        <br><br>
                        <?php if($beconnected_list->num_rows() > 0) : $i=1;?>
                        <div class="table-primary" id="tooltips-demo">
                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-example0">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Kontak</th>
                                        <th>Domisili</th>
                                        <th>Karya</th>
                                        <th>Link Karya</th>
                                        <th>Create Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($beconnected_list->result() as $row) :?>
                                    <tr class="odd gradeX">
                                        <td><?=$i++?></td>
                                        <td><?php echo $row->nama?><br>
                                            <?php echo $row->telp?><br>
                                            <?php echo $row->email?></td>
                                        <td><?php echo $row->domisili?></td>
                                        <td><?php echo $row->pilihan_karya?></td>
                                        <td><?php echo nl2br($row->link_karya)?></td>
                                        <td>
                                            <h5><?php echo date('d F Y', strtotime($row->create_date))?><br>
                                            <small>Pukul <?php echo date('h:i:s', strtotime($row->create_date))?> WIB</small></h5>
                                        </td>
                                    </tr>
                                <?php endforeach;?>   
                                </tbody>
                            </table>
                        </div>
                        <?php else :?>

                            <div class="alert">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>Warning!</strong> Data tidak ada</div>

                        <?php endif;?>
                    </div>
                </div>
<!-- /11. $JQUERY_DATA_TABLES -->

            </div>
        </div>



    </div> <!-- / #content-wrapper -->
    <div id="main-menu-bg"></div>
</div> <!-- / #main-wrapper -->
<?php $this->load->view('admin/page_form_script_below'); ?>

