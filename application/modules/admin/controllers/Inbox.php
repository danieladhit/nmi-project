<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 *
 * @author	M.Fadli Prathama (09081003031)
 *  email : m.fadliprathama@gmail.com
 *
 */
class inbox extends CMS_Controller {
	
	public function index()
	{
		if($this->session->userdata('login_admin') == true or $this->session->userdata('login_editor') == true or $this->session->userdata('login_content') == true)
		{
			$user_id 				= $this->session->userdata('id_user');
			$judul					= $this->model_utama->get_detail('1','setting_id','setting')->row()->website_name;
			$data['title'] 			= 'Halaman Kotak Masuk | '.$judul;
			$data['heading'] 		= "Kotak Masuk";
			$data['page']			= 'admin/page_kotak_masuk';
			$data['suara_list']	= $this->model_utama->get_order('create_date','desc','suara');
			$this->load->view('admin/template', $data);
			$log['user_id']			= $this->session->userdata('id_user');
			$log['activity']		= "lihat kotak masuk";
			$this->model_utama->insert_data('log_user', $log);
		}
		else
		{
			redirect('login');
		}
	}

	function delete($kode)
	{
		if($this->session->userdata('login_admin') == true or $this->session->userdata('login_editor') == true)
		{
			$log['user_id']				= $this->session->userdata('id_user');
			$log['activity']			= 'hapus data inbox dengan id : '.$kode.'  ';
			$this->model_utama->insert_data('log_user', $log);
			$this->model_utama->delete_data($kode, 'suara_id','suara');
			$this->session->set_flashdata('success', 'Data berhasil dihapus!');
			redirect('admin/inbox');
		}
		else
		{
			redirect('login');
		}
	}
}
