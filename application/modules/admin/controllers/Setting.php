<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 *
 * @author	M.Fadli Prathama (09081003031)
 *  email : m.fadliprathama@gmail.com
 *
 */
class setting extends CMS_Controller {
	

	public function index(){

		if(		$this->session->userdata('login_admin') == true 
			or 	$this->session->userdata('login_editor') == true
		){
			if( isset($_FILES['userfile']) ){
				$config['upload_path'] 		= './uploads/logo/';
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg|doc|docx|xls|xlsx|rar|zip';
				
				$image_folder_path 			= 'uploads/logo/thumb';
				$file_dokumen 				= $this->upload_photo( 	$image_folder_path,
																	$config );

				if( $file_dokumen != '' ){
					$weleh = array (
									'logo' 			=> $this->security->xss_clean($file_dokumen)
									);
				}
				
				$this->model_utama->update_data('1','setting_id','setting',$weleh);
			}
		
			$user_id 						= $this->session->userdata('id_user');
			$judul							= $this->model_utama->get_detail('1','setting_id','setting')->row()->website_name;
			$data['title'] 					= 'Halaman Kelola page | '.$judul;
			$data['heading'] 				= "Setting";
			$data['page']					= 'admin/setting/form';
			$data['default']				= $this->model_utama->cek_data('1','setting_id','setting')->row_array();
			$data['feature_settings_list']	= $this->model_utama->get_data('feature_settings');
			$data['setting_home_list']		= $this->model_utama->get_data('setting_home');
			$data['list_page']				= $this->db->query("select *,page.page_id as id_page from page left join setting_home_product on setting_home_product.page_id = page.page_id where setting_home_product.page_id is null");

			$data['list_page_popular']		= $this->db->query("select *,page.page_id as id_page from page left join setting_popular_content on setting_popular_content.page_id = page.page_id where setting_popular_content.page_id is null");

			$data['our_product']			= $this->db->query("select * from setting_home_product,page where setting_home_product.page_id = page.page_id");

			$data['our_product_popular']			= $this->db->query("select * from setting_popular_content,page where setting_popular_content.page_id = page.page_id");

			$data['halaman_tambahan']		= $this->db->query("select * from setting_tambahan_content,page where setting_tambahan_content.page_id = page.page_id");

			$data['template_list']			= $this->model_utama->get_data('templates');
			$data['template_list_active']		= $this->db->query('SELECT * FROM templates WHERE active="1"');
			$this->load->view('admin/template', $data);
			
			// $this->log_activity("lihat setting");
		}else{
			redirect(base_url().'login');
		}
	}
	
	

	function update_field($field,$id,$id_field,$table)
	{
		if($this->session->userdata('login_admin') == true)
		{

			$data[$field] = $this->input->post('value');
			if($field ==  $table.'_title')
			{
				$slug 	= $table.'_slug';
				$query 	= $this->db->query("SHOW COLUMNS FROM `$table` LIKE '$slug'");
				if($query->num_rows() > 0) {
					$data[$table.'_slug'] = url_title($this->input->post('value'),'dash',true);
				}
				$query_date	= $this->db->query("SHOW COLUMNS FROM `$table` LIKE 'update_date'");
				if($query_date->num_rows() > 0) {
					$data['update_date'] = date('Y-m-d H:i:s');
				}
			}

			$this->model_utama->update_data($id,$id_field,$table, $data);
			
			// $this->load->library('create_html_file_library');
			// $this->create_html_file_library->delete_all_html_file();
		
		}
	}
	
	function add_halaman()
	{
		$page_id	= $this->input->post("page_id");
		
		$data["page_id"]		= $page_id;
		$data["create_date"]	= date("Y-m-d");
		
		$this->model_utama->insert_data("setting_home_product", $data);
		
		$our_product		= $this->db->query("select *,page.page_id as id_page from page left join setting_home_product on setting_home_product.page_id = page.page_id where setting_home_product.page_id is null");
		
		$list_page		= '<option value="">-- Pilih Halaman --</option>';
	
		foreach($our_product->result() as $row)
		{
			$list_page	.= '<option value="'.$row->id_page.'">'.$row->page_title.'</option>';
		}

		$query_fresh_add	= $this->db->query("select * from page,setting_home_product where page.page_id = '$page_id' and setting_home_product.page_id = page.page_id")->row();
		
		$fresh_add	=	'
				<br>
				<div class="row" id="halaman-selected-'.$query_fresh_add->page_id.'">
					<div class="col-md-6">
						<input type="text" class="form-control" value="'.$query_fresh_add->page_title.'" disabled/> 
					</div>
					<div class="col-md-2">
						<button class="btn btn-danger" onclick="hapusHalaman('.$query_fresh_add->setting_home_product_id.')">Hapus</button>
					</div>
				</div>';
						
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array(
										'list_page' => $list_page,
										'fresh_add' => $fresh_add
									)));
				
	}
	
	function hapus_halaman()
	{
		$page_id		= $this->input->post('page_id');
		
		$this->db->query("delete from setting_home_product where page_id = '$page_id'");
		
		$our_product		= $this->db->query("select *,page.page_id as id_page from page left join setting_home_product on setting_home_product.page_id = page.page_id where setting_home_product.page_id is null");
		
		$list_page		= '<option value="">-- Pilih Halaman --</option>';
	
		foreach($our_product->result() as $row)
		{
			$list_page	.= '<option value="'.$row->page_id.'">'.$row->page_title.'</option>';
		}	
		
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array(
										'list_page' => $list_page
									)));
	}

	// for popular content 
	function add_halaman2()
	{
		$page_id	= $this->input->post("page_id");
		
		$data["page_id"]		= $page_id;
		$data["create_date"]	= date("Y-m-d");
		
		$this->model_utama->insert_data("setting_popular_content", $data);
		
		$our_product		= $this->db->query("select *,page.page_id as id_page from page left join setting_popular_content on setting_popular_content.page_id = page.page_id where setting_popular_content.page_id is null");
		
		$list_page		= '<option value="">-- Pilih Halaman --</option>';
	
		foreach($our_product->result() as $row)
		{
			$list_page	.= '<option value="'.$row->id_page.'">'.$row->page_title.'</option>';
		}

		$query_fresh_add	= $this->db->query("select * from page,setting_popular_content where page.page_id = '$page_id' and setting_popular_content.page_id = page.page_id")->row();
		
		$fresh_add	=	'
				<br>
				<div class="row" id="halaman-selected-'.$query_fresh_add->page_id.'">
					<div class="col-md-6">
						<input type="text" class="form-control" value="'.$query_fresh_add->page_title.'" disabled/> 
					</div>
					<div class="col-md-2">
						<button class="btn btn-danger" onclick="hapusHalaman('.$query_fresh_add->setting_popular_content_id.')">Hapus</button>
					</div>
				</div>';
						
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array(
										'list_page' => $list_page,
										'fresh_add' => $fresh_add
									)));
				
	}
	
	function hapus_halaman2()
	{
		$page_id		= $this->input->post('page_id');
		
		$this->db->query("delete from setting_popular_content where page_id = '$page_id'");
		
		$our_product		= $this->db->query("select *,page.page_id as id_page from page left join setting_popular_content on setting_popular_content.page_id = page.page_id where setting_popular_content.page_id is null");
		
		$list_page		= '<option value="">-- Pilih Halaman --</option>';
	
		foreach($our_product->result() as $row)
		{
			$list_page	.= '<option value="'.$row->page_id.'">'.$row->page_title.'</option>';
		}	
		
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array(
										'list_page' => $list_page
									)));
	}

	// for halaman content 
	function add_halaman3()
	{
		$page_id	= $this->input->post("page_id");
		
		$data["page_id"]		= $page_id;
		$data["create_date"]	= date("Y-m-d");
		
		if ($page_id != null || $page_id != '')
		{
			$this->model_utama->insert_data("setting_tambahan_content", $data);
		}
		
		$our_product		= $this->db->query("select *,page.page_id as id_page from page left join setting_tambahan_content on setting_tambahan_content.page_id = page.page_id where setting_tambahan_content.page_id is null");
		
		$list_page		= '<option value="">-- Pilih Halaman --</option>';
	
		foreach($our_product->result() as $row)
		{
			$list_page	.= '<option value="'.$row->id_page.'">'.$row->page_title.'</option>';
		}

		$query_fresh_add	= $this->db->query("select * from page,setting_tambahan_content where page.page_id = '$page_id' and setting_tambahan_content.page_id = page.page_id")->row();
		
		$fresh_add	=	'
				<br>
				<div class="row" id="halaman-selected-'.$query_fresh_add->page_id.'">
					<div class="col-md-6">
						<input type="text" class="form-control" value="'.$query_fresh_add->page_title.'" disabled/> 
					</div>
					<div class="col-md-2">
						<button class="btn btn-danger" onclick="hapusHalaman('.$query_fresh_add->setting_tambahan_content_id.')">Hapus</button>
					</div>
				</div>';
						
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array(
										'list_page' => $list_page,
										'fresh_add' => $fresh_add
									)));
				
	}
	
	function hapus_halaman3()
	{
		$page_id		= $this->input->post('page_id');
		
		$this->db->query("delete from setting_tambahan_content where page_id = '$page_id'");
		
		$our_product		= $this->db->query("select *,page.page_id as id_page from page left join setting_tambahan_content on setting_tambahan_content.page_id = page.page_id where setting_tambahan_content.page_id is null");
		
		$list_page		= '<option value="">-- Pilih Halaman --</option>';
	
		foreach($our_product->result() as $row)
		{
			$list_page	.= '<option value="'.$row->page_id.'">'.$row->page_title.'</option>';
		}	
		
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array(
										'list_page' => $list_page
									)));
	}
	
	function update_themes(){
		$this->db->query("UPDATE templates SET active='0'");
		$this->db->query("UPDATE templates SET active='1' WHERE template_id=".$this->security->xss_clean($this->input->post('value')) );
	}
}

