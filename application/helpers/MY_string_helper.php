<?php

/**
 * Helper class to work with string
 */

// check whether a string starts with the target substring
function starts_with($haystack, $needle)
{
	return substr($haystack, 0, strlen($needle))===$needle;
}

// check whether a string ends with the target substring
function ends_with($haystack, $needle)
{
	return substr($haystack, -strlen($needle))===$needle;
}

function youtube_id($youtube_link = '')
{
	$id = '';
	$url = $youtube_link;
	preg_match(
		'/[\\?\\&]v=([^\\?\\&]+)/',
		$url,
		$matches
		);
	if( count($matches) > 0 ){
		$id = $matches[1];
	}
	return $id;
}

function get_iframe_src($iframe_string)
{
	$url = '';
	preg_match('/src="([^"]+)"/', $iframe_string, $matches);
	if( count($matches) > 0 ){
		$url = $matches[1];
	}
	return $url;
}

// function to get  the address
function get_lat_long($url){

	preg_match('/@(\-?[0-9]+\.[0-9]+),(\-?[0-9]+\.[0-9]+)/', $url, $matches );
	$lat=(float)$matches[1];
	$long=(float)$matches[2];
	return $lat.','.$long;
}

/*

* Getting User Agent

*/
function get_user_agent()
{
	$ci = &get_instance();

	$ci->load->library('user_agent');

	if ($ci->agent->is_browser())
	{
		$agent = $ci->agent->browser().' '.$ci->agent->version();
	}
	elseif ($ci->agent->is_robot())
	{
		$agent = $ci->agent->robot();
	}
	elseif ($ci->agent->is_mobile())
	{
		$agent = $ci->agent->mobile();
	}
	else
	{
		$agent = 'Unidentified User Agent';
	}

	$platform = $ci->agent->platform(); // Platform info (Windows, Linux, Mac, etc.)

	return $platform.' '.$agent;
}

